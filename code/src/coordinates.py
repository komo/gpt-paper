"""
providing static r and z coordinates both for 42x23 and 85x47 pixel resolutions
and functions to load magnetic coordinates for given shotnumber and time
"""
from static_coords import *
import sys
sys.path.append('/afs/ipp/aug/ads-diags/common/python/lib')
import map_eq_gharr as me
import warnings


#TODO solve problem with NaN values for theta 
#TODO improve performance (one call takes about 4 seconds)

def get_magnetic_coords(shotnumber: int, time: float, 
                        fullsize: bool=False) -> [np.ndarray, np.ndarray]:
    '''
    Returns rho and theta coordinates for the given shotnumber and time;
    Fullsize specifies the resolution (42x23 if False, 85x47 if True)
    '''
    with warnings.catch_warnings():
        warnings.simplefilter("ignore") # remove DeprecationWarning 
        eq = me.equ_map()
        eq.open(shotnumber)
        rs = rs_full if fullsize else rs_small
        zs = zs_full if fullsize else zs_small
        
        rho = np.squeeze(eq.rz2rho(np.atleast_3d(rs.T).T, 
                                    np.atleast_3d(zs.T).T, time))
 
        r0,z0 = eq.rho2rz(0.3,time)
        
        # if not len(r0[0][0]) or not len(z0[0][0]):
        #     # for shot 36173 for example eq.rho2rz(0.1,time) return empty lists
        #     # resulting in a array full on NaN values for theta.
        #     r0,z0 = eq.rho2rz(0.1,time)
        r0,z0 = r0[0][0].mean(), z0[0][0].mean()
        if np.isnan(r0):
            print("NaN")
        theta = np.arctan2(zs-z0,rs-r0)
        return rho, theta


def open_meq_for_shot(shotnumber: int) -> me.equ_map:
    '''returns opened equ_map object'''
    eq = me.equ_map()
    eq.open(shotnumber)
    return eq


# this function is slightly faster and can be used if the coordinates have 
# to be calculated for many timepoints of the same shot
def get_magnetic_coords_fast(eq_open: me.equ_map, 
                             time: float) -> (np.ndarray, np.ndarray):
    '''
    Same as get_magnetic_coords but takes opened equ_map object as input.
    Slightly faster if one has to calculate magnetic coords for several 
    timepoints of the same shotnumber
    '''
    rs = rs_small
    zs = zs_small
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")  # remove DeprecationWarning 
        rho = np.squeeze(eq_open.rz2rho(np.atleast_3d(rs.T).T, 
                                    np.atleast_3d(zs.T).T, time))
        r0,z0 = eq_open.rho2rz(0.3,time)
        # if not len(r0[0][0]) or not len(z0[0][0]):
        #     # for shot 36173 for example eq.rho2rz(0.1,time) return empty lists
        #     # resulting in a array full on NaN values for theta.
        #     print("bla")
        #     r0,z0 = eq_open.rho2rz(0.2,time)
        r0,z0 = r0[0][0].mean(), z0[0][0].mean()
        if np.isnan(r0):
            print("  NaN at t = {}".format(time))
        theta = np.arctan2(zs-z0,rs-r0)
        return rho, theta


def get_magnetic_time_series(shotnumber: int, 
                             time_points: np.ndarray) -> np.ndarray:
    '''
    takes shotnumber and array of timepoints in seconds as input returns 
    array with magnetic coords (42x23 pixels) for those timepoints
    '''
    eq_open = open_meq_for_shot(shotnumber)
    magnetic_coords = []
    for i, t in enumerate(time_points):
        print("\rObtaining magnetic coordinates from source for frame {} of {}"
                .format(i+1, time_points.size), end="")
        magnetic_coords.append(get_magnetic_coords_fast(eq_open, t))
    print("")
    return np.array(magnetic_coords)