\section{Results}

For testing a dataset of 221 emission distributions was available. The dataset 
consisted of 75 existing tomograms, which were calculated with an iterative 
algorithm described in \cite{AUGconferencePaper1994, pierresPaper2}. The remaining emission distributions 
in the dataset consisted of completely artificial distributions like gaussian distributions or 
the emission being completely smeared out along the flux surfaces and modified 
tomograms. 

\subsection{Results for Data from Forward Model}

To show the capabilities of the approach taken in this paper the algorithm was 
first tested with artificial bolometer measurements that were calculated by applying 
the forward model to the dataset of emission distributions. The parameters for 
the kernel functions were obtained by using the common method of maximizing the 
logarithm of the marginal likelihood with regards to the hyperparameters of the 
selected kernel function \cite{Rasmussen06}.

\begin{equation}\label{kap3_eq_log_like}
    \ln(p(\vec{P}|\vec{\theta})) = 
    - \frac{1}{2}\ln(\det[\mathbf{\Sigma}_P + \mathbf{S\Sigma}_E\mathbf{S}^\mathrm{T}]) 
    - \frac{1}{2}(\vec{P}-\mathbf{S}\vec{\mu}_E)^\mathrm{T}[\mathbf{\Sigma}_P + 
    \mathbf{S\Sigma}_E\mathbf{S}^\mathrm{T}]^{-1}(\vec{P}-\mathbf{S}\vec{\mu}_E)
\end{equation}

The optimization was done with the implementation of 
the Nelder-Mead algorithm \cite{NelderMeadPaper} in the SciPy Python package.

Both the non-stationary cartesian 
kernel function \(k_{\text{var\_l}}\) and a combined kernel function
\(k_{\text{comb}} = k_{\text{var\_l}} + k_{\text{meq}}\) incorporating both 
the non stationary cartesian and the magnetic equilibrium kernel function were tested.
The latter is possible since a kernel matrix is by definition symmetric 
and positive-semidefinite and an addition of two such matrices still fulfills 
those conditions.
This gives three and six hyperparameters, that have to be optimized 
\begin{align}
    &\vec{\theta}_{\mathrm{var\_l}} = (\sigma_{\text{var\_l}},\ l_0,\ \delta l)\\
    &\vec{\theta}_{\mathrm{comb}} = (\sigma_{\text{var\_l}},\ l_0,\ \delta l,\ 
    \sigma_{\text{meq}},\ l_{\rho},\ l_{\theta})
\end{align}
The ratio of the two sigmas in the later kernel function can be 
interpreted as the relative contribution to the combined kernel of one kernel 
compared to the other.

\begin{figure}
\centering
\begin{subfigure}{.49\linewidth}
    \centering
    \includegraphics[width=\linewidth]{fit_synth94_2.png}
    \subcaption{}
    \label{img_fit94}
\end{subfigure}
\begin{subfigure}{.49\linewidth}
    \centering
    \includegraphics[width=\linewidth]{fit_synth208_2.png}
    \subcaption{}
    \label{img_fit208}
\end{subfigure}
\caption{ \textbf{a)} Fits for an artificial sample emission. When the emission 
is strongly correlated to the magnetic flux like the ring in the center, the 
combined kernel is better at reconstructing the structure. \textbf{b)} Fits for 
a sample emission calculated from real data with another algorithm. For realistic 
emission distributions the differences between the cartesian and the combined kernel is 
far more subtle.}
\label{img_fits_forward}
\end{figure}
    

Two examples are shown in figure \ref{img_fits_forward}. Figure \ref{img_fit94} 
shows the result for an artificial emission distribution with a ring along the 
flux lines in the center. In cases like this, where the emission distribution is at least 
in some regions strongly correlated to the magnetic flux, the combined kernel is able to 
reproduce those structures better.
Figure \ref{img_fit208} on the other hand shows the fits for an emission distribution that 
was obtained from real bolometer data with another algorithm. For such realistic emission
distributions the differences are more subtle. 

One advantage of Gaussian process regression is that one can immediately obtain the 
error of the fit from the posterior emission distribution. In cases 
where the ground truth (sample emission) is known this can be used to quantify the 
quality of the fit. Figure \ref{img_residuum208} shows the absolute difference 
between the sample emission and the fit divided by the standard deviation of 
the posterior emission distribution for both the cartesian and the combined kernel. 
While a few individual pixels have a deviation of up to 4 standard deviations, the 
average deviation of the pixels within the vessel is close to 0.5 std in both cases.

Another way of comparing sample emission and fit is via the forward model. Figure 
\ref{img_forward208} shows the bolometer measurements of the sample emission via 
the forward model with the expected error from the error model compared to the 
forward model measurements of the fit with the cartesian and combined kernel. 
Both fits are within one standard deviation of the sample emission measurement.

This shows that the approach chosen in this paper is able to reproduce existing emission
distributions to a high accuracy given that the right parameters for the kernel function 
are selected. The difference between the cartesian and combined kernel is significant for 
artificial edge cases but negligible for realistic scenarios.

It is noteworthy that especially for the combined kernel with 6 parameters to 
optimize, maximizing the log likelihood is not always reliable and requires selecting 
good initial parameters in order to prevent ending up in local maxima.

\begin{figure}
    \centering
    \includegraphics[width=0.5\linewidth]{residuum208.png}
    \caption{Absolute difference between sample emission and fit divided by the 
    standard deviation of the posterior emission distribution for sample 208
    shown in figure \ref{img_fit208}.}
    \label{img_residuum208}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\linewidth]{forward_meas208_2.png}
    \caption{The black dots show the measurement obtained by applying the forward model.
            The measurement error comes from the error model described in 
            \ref{section_preprocessing}. Both the measurement and the error 
            are used to compute fits with the non stationary cartesian and the 
            combined kernel. The reason for the large error bars of some channels
            compared to others is that those channels have a much higher 
            background noise.}
    \label{img_forward208}
\end{figure}