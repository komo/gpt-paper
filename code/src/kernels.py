"""Different kernel/covariance functions that can be used"""

import numpy as np

# Available kernel/covariance functions
KERNEL_CARTESIAN = "cartesian"
KERNEL_CARTESIAN_VAR_Z = "cartesian_var_z"
KERNEL_MAGNETIC = "magnetic"
KERNEL_COMBINED = "combined"
KERNEL_COMBINED_VAR_Z = "combined_var_z"


def local_lengthscale(z: np.ndarray, params: np.ndarray, 
                      x_point_up: bool = False) -> np.ndarray:
    """
    Returns local lengthscale along z axis. 
    Used for nonstationary cartesian kernel
    """
    l0, dl = params # base value, variation value 
    z0 = 0.75 if x_point_up else -0.75
    dz = -0.2 if x_point_up else 0.2
    return l0 + dl/2*(1+np.tanh((z-z0)/dz))


def k_cartesian(pts: np.ndarray, params: np.ndarray) -> np.ndarray:
    """Simple stationary cartesian kernel"""
    sigma1, lr1 = params 
    dR = pts[:, 0, np.newaxis] - pts[np.newaxis, :, 0]
    dz = pts[:, 1, np.newaxis] - pts[np.newaxis, :, 1] 
    to_return  = sigma1**2*np.exp(-(dR**2 + dz**2)/(2*lr1**2))
    return to_return + 0.005 * params[0]*np.identity(to_return.shape[0])


def k_magnetic(pts: np.ndarray, params: np.ndarray) -> np.ndarray:
    """
    Simple magnetic kernel (distance between pixels measurend according 
    to magnetic coordinates rho and theta)
    """
    sigma2, lrho, ltheta = params
    drho = pts[:, 2, np.newaxis] - pts[np.newaxis, :, 2]
    dtheta = (pts[:, 3, np.newaxis] - pts[np.newaxis, :, 3])
    dtheta = (dtheta + np.pi) % (2*np.pi) - np.pi
    
    to_return = (sigma2**2*np.exp(-(drho**2)/(2*lrho**2)) * 
                    np.exp(-(dtheta**2)/(2*ltheta**2)))
  
    return to_return + 0.01 * params[0]*np.identity(to_return.shape[0])


def k_cartesian_var_z(pts: np.ndarray, params: np.ndarray, 
                      x_point_up: bool) -> np.ndarray:
    """Cartesian kernel with nonstationary lengthscale along z-axis"""
    sigma1, lv_params = params[0], params[1:]
    lv1 = local_lengthscale(pts[:,1][::-1], lv_params, x_point_up)
    lv2 = local_lengthscale(pts[:,1][::-1], lv_params, x_point_up)
    dR = pts[:, 0, np.newaxis] - pts[np.newaxis, :, 0]
    dz = pts[:, 1, np.newaxis] - pts[np.newaxis, :, 1]
    local_l = lv1[:, np.newaxis]**2 + lv2[np.newaxis, :]**2

    to_return  = sigma1**2*np.exp(-(dR**2 + dz**2)/(local_l)) 
    return to_return + 0.02 * params[0]*np.identity(to_return.shape[0])


def k_combined(pts: np.ndarray, params: np.ndarray) -> np.ndarray:
    """combination of cartesian and magnetic kernel"""
    return k_cartesian(pts, params[:2]) + k_magnetic(pts, params[2:])


def k_combined_var_z(pts: np.ndarray, params: np.ndarray, 
                     x_point_up: bool = False) -> np.ndarray:
    """combinations of nonstationary cartesian and magnetic kernel"""
    return (k_cartesian_var_z(pts, params[:3], x_point_up) + 
                            k_magnetic(pts, params[3:]))



# Experimental Kernels

# def local_lengthscale_rho(rho: np.ndarray, params: np.ndarray) -> np.ndarray:
#     l_rho_center, dl = params
#     return l_rho_center - dl*rho**2


# def local_lengthscale_theta(rho: np.ndarray, params: np.ndarray) -> np.ndarray:
#     l_theta_center, dl = params
#     return l_theta_center - dl*rho**3


# def k_magnetic_var_rho(pts1: np.ndarray, pts2: np.ndarray, 
#                       params: np.ndarray) -> np.ndarray:

#     sigma1, l_rho_params, ltheta = params[0], params[1:-1], params[-1]

#     lrho1 = local_lengthscale_rho(pts1[:,2][::-1], l_rho_params)
#     lrho2 = local_lengthscale_rho(pts2[:,2][::-1], l_rho_params)
    
#     drho = pts1[:, 2, np.newaxis] - pts2[np.newaxis, :, 2]
#     dtheta = (pts1[:, 3, np.newaxis] - pts2[np.newaxis, :, 3])
#     dtheta = (dtheta + np.pi) % (2*np.pi) - np.pi
    
#     local_l_rho = lrho1[:, np.newaxis]**2 + lrho2[np.newaxis, :]**2

#     to_return  = (sigma1**2*np.exp(-(drho**2)/(local_l_rho)) *
#                     np.exp(-(dtheta**2)/(2*ltheta**2)))
#     return to_return + 0.005 * params[0]*np.identity(to_return.shape[0])


# def k_magnetic_var_theta(pts1: np.ndarray, pts2: np.ndarray, 
#                         params: np.ndarray) -> np.ndarray:

#     sigma1, l_rho, l_theta_params = params[0], params[1:2], params[2:]

#     ltheta1 = local_lengthscale_rho(pts1[:,3][::-1], l_theta_params)
#     ltheta2 = local_lengthscale_rho(pts2[:,3][::-1], l_theta_params)
    
#     drho = pts1[:, 2, np.newaxis] - pts2[np.newaxis, :, 2]
#     dtheta = (pts1[:, 3, np.newaxis] - pts2[np.newaxis, :, 3])
#     dtheta = (dtheta + np.pi) % (2*np.pi) - np.pi
    
#     local_l_theta = ltheta1[:, np.newaxis]**2 + ltheta2[np.newaxis, :]**2

#     to_return  = (sigma1**2*np.exp(-(drho**2)/(2*l_rho**2)) *
#                     np.exp(-(dtheta**2)/(local_l_theta)))
#     return to_return + 0.005 * params[0]*np.identity(to_return.shape[0])


# def k_magnetic_var_rhotheta(pts1: np.ndarray, pts2: np.ndarray, 
#                                 params: np.ndarray) -> np.ndarray:

#     sigma1, l_rho_params, l_theta_params = params[0], params[1:3], params[3:]

#     lrho1 = local_lengthscale_rho(pts1[:,2][::-1], l_rho_params)
#     lrho2 = local_lengthscale_rho(pts2[:,2][::-1], l_rho_params)
#     ltheta1 = local_lengthscale_rho(pts1[:,3][::-1], l_theta_params)
#     ltheta2 = local_lengthscale_rho(pts2[:,3][::-1], l_theta_params)
    
#     drho = pts1[:, 2, np.newaxis] - pts2[np.newaxis, :, 2]
#     dtheta = (pts1[:, 3, np.newaxis] - pts2[np.newaxis, :, 3])
#     dtheta = (dtheta + np.pi) % (2*np.pi) - np.pi
    
#     local_l_rho = lrho1[:, np.newaxis]**2 + lrho2[np.newaxis, :]**2
#     local_l_theta = ltheta1[:, np.newaxis]**2 + ltheta2[np.newaxis, :]**2

#     to_return  = (sigma1**2*np.exp(-(drho**2)/(local_l_rho)) *
#                     np.exp(-(dtheta**2)/(local_l_theta)))
#     return to_return + 0.005 * params[0]*np.identity(to_return.shape[0])


# def k_combined_var_zrho(pts1: np.ndarray, pts2: np.ndarray, 
#                         params: np.ndarray, 
#                         x_point_up: bool = False) -> np.ndarray:

#     return (k_cartesian_var_z(pts1, pts2, params[:3], x_point_up) + 
#                             k_magnetic_var_rho(pts1, pts2, params[3:]))


# def k_combined_var_everything(  pts1: np.ndarray, pts2: np.ndarray, 
#                                 params: np.ndarray, 
#                                 x_point_up: bool = False) -> np.ndarray:
                                
#     return (k_cartesian_var_z(pts1, pts2, params[:3], x_point_up) + 
#                             k_magnetic_var_rhotheta(pts1, pts2, params[3:]))
