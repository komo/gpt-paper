"""Metrics for comparing training samples with predictions"""

from typing import Dict
import numpy as np
from skimage.metrics import structural_similarity as ssim
from skimage.metrics import peak_signal_noise_ratio as psnr
import sys
from pathlib import Path
sys.path.append(str(Path(__file__).parent.absolute()))
from static_coords import r_coord, r_coord_full
from constants import mask

def nrmse(original: np.ndarray, fit: np.ndarray) -> float:
    """Normalized root mean squared error"""
    error = np.linalg.norm(original-fit)
    norm = np.linalg.norm(original)
    return error/norm


def integrate_emission(emission: np.ndarray) -> float:
    """returns integrated total emission power"""
    r = r_coord if emission.size == 966 else r_coord_full
    # approx 5cm per pixel
    return (2*np.pi*r_coord*emission).sum()/(1000000)*0.05**2 


def rdte(original: np.ndarray, fit: np.ndarray) -> float:
    """Relative deviation in total emission"""
    return (np.abs(integrate_emission(original)-integrate_emission(fit))/
            integrate_emission(original))


def residuum(original, fit, posterior_cov):
    """
    returns absolute error between original and fit divided by std from 
    posterior covariance
    """
    std = np.sqrt(posterior_cov) + np.logical_not(mask)
    res = np.abs(fit-original)/std
    res[res>100] = 0.01 # a few single pixels have huge values, probably because std there is small. This is regarded as numerical error
    return res


def residuum_mean(original, fig, posterior_cov):
    """returns mean of pixels inside vessel for residuum"""
    res = residuum(original, fig, posterior_cov)
    return res[mask.astype(bool)].mean()


def signal_to_noise(fit, posterior_cov):
    std = np.sqrt(posterior_cov) + np.logical_not(mask)
    return np.abs(fit)/std


def signal_to_noise_mean(fit, posterior_cov):
    res = signal_to_noise(fit, posterior_cov)
    return res[mask.astype(bool)].mean()


def get_metrics(emission1: np.ndarray, emission2: np.ndarray, 
                additional_info: Dict = {}) -> Dict:
    nrmse_value = nrmse(emission1, emission2)
    ssim_value = ssim(emission1, emission2, 
                    data_range=emission1.max()-emission2.min())
    psnr_value = psnr(emission1, emission2, 
                    data_range=emission1.max()-emission2.min())
    rdte_value = rdte(emission1, emission2)

    metric_values = {"NRMSE": nrmse_value, "SSIM": ssim_value, 
                        "PSNR": psnr_value, "RDTE": rdte_value}
    return {**additional_info, **metric_values}

    