\section{Additional Data Processing}

In order to use real bolometer measurements they are first 
analyzed to characterize the measurement noise before performing the GPT calculation. 
Similarly some processing steps are performed 
after the GPT calculation to handle the vessel boundaries and negative values 
in the emission distribution.


\subsection{Preprocessing}\label{section_preprocessing}

To calculate the emission distribution from real data for a given shot and 
time point first the bolometer measurements for the whole shot are loaded.
In order to reduce the signal noise of the bolometers for the GPT 
calculation the same low pass filter that was used in \cite{bachelor} for the computation 
of the error model mentioned in \ref{section_postprocessing} 
is applied to the signal of all channels. 

Before the signal at the desired time point is used as input for the GPT, 
it is checked if any of the 128 data points of the low pass signal is 0 or 
negative. Channels where this is the case are neglected, by 
setting the respective row in the transfer matrix to 0. This is done for two reasons. 
The first is to exclude channels that are inactive and only record zeros, the 
second is to get rid of outliers from channels that are drifting and record significant
negative values. Strictly speaking excluding negative values which are compatible with 
zero introduces a bias in the likelihood, which is however
considered not relevant for this proof of principle and therefore neglected.
In the large majority of cases channels were neglected due to being inactive.
To obtain the measurement error an error model is used where the error of 
each channel is assumed to be given by a constant background noise and a signal 
dependent noise. For the variance \(\sigma^2_i\) of the \(i\)-th channel we get 
\begin{equation}
\sigma^2_i = \sigma^2_{i,\mathrm{background}} + m_i\cdot P_i
\end{equation}
as variances are additive. Here \(\sigma_{i,\mathrm{background}}\) is 
the background noise for the \(i\)-th channel and \(m_i\) is a proportionality 
factor describing how the variance depends on the signal of the channel. 
Both were obtained by analyzing the bolometer signals of 1793 shots \cite{bachelor}.


\subsection{Postprocessing}\label{section_postprocessing}

The Gaussian process has no knowledge of the vessel boundaries. As the transfer 
matrix is 0 for pixels outside of the vessel, the fitted emission distribution 
tends to go to 0 there. Due to the finite length scale of the kernel this does not 
always happen fast enough. Figure \ref{img_vessel_bleeding} shows an example where 
some emissivity is outside of the vessel. 
To ensure that pixels outside are 0 the fitted emission 
distribution gets multiplied with a binary mask resembling the vessel shape. 
\begin{figure}
    \centering
    \includegraphics[width=.5\linewidth]{vessel_boundary.png}
    \caption{Result of GPT calculation with real data for shot 36173 at 
            t = 6 s before multiplying with the binary mask, encoding
            the vessel shape. Especially in the lower right quadrant it is 
            visible that the emission distribution bleeds outside the vessel.}
    \label{img_vessel_bleeding}
\end{figure}

The Gaussian process also has no boundaries. Hence it is possible that 
the result from the GPT has negative values for some pixels. As this is 
physically not possible, negative pixels are set to 0 and the whole 
emission distribution is renormalized to have the same total emission power 
integrated over the whole vessel volume as before. 
When looking at the results of the GPT for all 75 
samples from real data with the static parameters obtained in \ref{section_static}
and the forward model as input, the ratio of total integrated emission and 
integrated negative emission is \((0.98 \pm 1.08)\%\). The maximum value for 
the ratio is \(5.59\%\). Hence the amplitude of the emission distribution 
is expected to be lowered by a few percent, while the shape of the distribution 
is not affected by the renormalization process.