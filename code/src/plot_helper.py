"""Some functions for visualizing results"""

import sys
from pathlib import Path
sys.path.append(str(Path(__file__).parent.absolute()))
import numpy as np 
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from pathlib import Path
import matplotlib.gridspec as gridspec
from mpl_toolkits.axes_grid1 import make_axes_locatable, ImageGrid
from static_coords import r_coord, r_coord_full, z_coord, z_coord_full


plt.rcParams['figure.facecolor'] = 'white'

data_dir = Path(__file__).parents[1] / "data"

small_mask = np.loadtxt("data/mask.txt")
full_mask = np.loadtxt("data/mask_fullres.txt")

time_indicator = None


def plot_saver(path, dpi=200, transparent=False):
    """saves plot at specified path"""
    plt.savefig(path, bbox_inches='tight', dpi=dpi, transparent=transparent)


def plot_emission(  emission: np.ndarray, 
                    fig: plt.figure = None, 
                    ax: plt.axis = None, 
                    title: str = "", 
                    interpol: str = "spline36", 
                    prevent_negative: bool = False, 
                    limits: [float, float] = None, 
                    cmap: str = "viridis",
                    symmetric_plot: bool = False,
                    include_scale: bool = True,
                    custom_scale_label = "",
                    convert_to_megawatt = True):
    """
    Takes emission as 2D input and creates an imshow with the right axis
    label and ticks for given matplotlib figure and axis. 
    
    PARAMETERS
    ----------
    emission : np.ndarray
        emission distribution to be plotted
    fig : matplotlib figure
        optional, if none is specified a new one is generated
    ax :  matplotlib axis
        optional, if none is specified a new one is generated
    title : str
        optional, sets title of plot
    prevent_negative: bool
        set axis minimum to 0 if true
    limits : [float, float]
        specifies minimum and maximum of colorscale explicitly
    cmap : str
        colormap to be used. See Maplotlib documentation for further info
    symmetric_plot : bool
        if set to true a symmetric colorscale with 0 is used, which is 
        helpful to make negative parts of emission distribution visible
    include_scale : bool
        include colorbar in plot
    custom_scale_label : string
        specify label for colorscale (default Emission in MW/m³)
    convert_to_megawatt:
        divide values by 1000000
    """

    if not fig:
        fig, ax = plt.subplots(1,1)

    conversion_factor = 1000000 if convert_to_megawatt else 1

    fullsize = (emission.size != 966)

    ax.set_xlabel("r in m")
    ax.set_ylabel("z in m")
    ax.set_title(title)
    if prevent_negative:
        emission[emission<0] = 0
    if not limits:
        limits=[emission.min(), emission.max()]
    if np.isnan(emission).any():
        limits[1] = np.nan

    r = r_coord_full if fullsize else r_coord
    z = z_coord_full if fullsize else z_coord
    mask = full_mask if fullsize else small_mask
    
    contour_color = "black" if symmetric_plot else "white"
    colormap = "seismic" if symmetric_plot else cmap
    if symmetric_plot: 
        limits[1] = limits[1] if limits else emission.max()
        limits[0] = -limits[1]

    ax.contour(r, z, mask[::-1], levels=[0.5], colors=contour_color)
    img = ax.imshow(emission/conversion_factor, interpolation=interpol, 
                    vmin=limits[0]/conversion_factor, 
                    vmax=limits[1]/conversion_factor,
                    extent=[r_coord[0], r_coord[-1], z_coord[0], z_coord[-1]], 
                    cmap=colormap)
    if include_scale:
        label = (custom_scale_label if custom_scale_label 
                    else "Emission in MW/m³")
        fig.colorbar(img, ax=ax, label=label)


def save_time_series(path, frames, title, dt_frame=20, 
                     slowdown=1, static_scale=True):
    """
    Saves time series of fitted emissions as mp4/gif ... 

    PARAMETERS
    ----------
    path : str or Path object
        specifies path+filename. The ending of the filename also specifies the 
        output format (mp4 ...). ffmpeg package is required for mp4!
    frames : np.ndarray
        array with all the fitted emission distributions for the time series
    dt_frame : int
        frametimes in milliseconds
    slowdown : int
        slowdown = 2 would mean, that 2 seconds movie are 1 seconds real time
    static_scale : bool
        if true the colorscale is kept constant for the whole movie
    """

    frames[np.isnan(frames)] = 0
    max_abs = frames.max()/1e6

    fig, ax = plt.subplots(1,1)
    div = make_axes_locatable(ax)
    cax = div.append_axes('right', '5%', '5%')
    
    frame0 = frames[0]
    ax.set_xlabel("r in m")
    ax.set_ylabel("z in m")
    ax.set_title(title)
    ax.contour(r_coord, z_coord, small_mask[::-1], 
                levels=[0.5], colors="white")
    im = ax.imshow(frame0/1e6, interpolation="spline36",
                        vmin=0, 
                        vmax=max_abs if static_scale else frame0.max()/1e6,
                        extent=[r_coord[0], r_coord[-1], 
                                z_coord[0], z_coord[-1]])
    fig.colorbar(im, cax=cax, label="Emission in MW/m³")

    def animate(i):
        frame = frames[i]/1e6
        vmax = max_abs if static_scale else frame.max()
        im.set_data(frame)
        im.set_clim(0, vmax)

    ani = animation.FuncAnimation(fig, animate, interval=dt_frame*slowdown, 
                                    frames=frames.shape[0])
    print("Animation complete, start writing to file ...")
    ani.save(path, bitrate=-1, dpi=150)
    print("Complete")


# more special stuff below

def save_fancy_plot_animation(path, emission_frames, profile_frames, 
                                filtered_meas, shotnumber, dt_frame=20, 
                                slowdown=1):

    global time_indicator

    max_abs = (emission_frames.max()/1e6)*0.9

    fig = plt.figure(figsize=(11,5))
    fig.suptitle("Shot {} at t = {} s".format(shotnumber, round(0.0,1)))
    grid = gridspec.GridSpec(2,3)
    ax_profile = fig.add_subplot(grid[0,1:])
    ax_emission = fig.add_subplot(grid[:,0])
    ax_meas = fig.add_subplot(grid[1,1:])

    ax_meas.set_ylabel("Bolometer signal")
    ax_meas.set_xlabel("time in s")
    ax_meas.set_title("Bolometer measurements")
    ax_profile.set_xlabel("rho")
    ax_profile.set_title("Profile")

    ax_profile.plot(profile_frames[0][1], 
                                    profile_frames[0][0]/1000)
    ax_meas.plot(np.arange(filtered_meas.shape[0])/(1000/dt_frame), 
                                        filtered_meas)
    time_indicator = ax_meas.axvline(0, color="black")

    plt.tight_layout(rect=[0,0,0.9,1])
    
    frame0 = emission_frames[0]
    ax_emission.set_xlabel("r in m")
    ax_emission.set_ylabel("z in m")
    ax_emission.contour(r_coord, z_coord, small_mask[::-1], 
                        levels=[0.5], colors="white")
    im = ax_emission.imshow(frame0/1e6, interpolation="spline36",
                            vmin=0, 
                            vmax=max_abs,
                            extent=[r_coord[0], r_coord[-1], 
                                    z_coord[0], z_coord[-1]])
    div = make_axes_locatable(ax_emission)
    cax = div.append_axes('right', '5%', '5%')
    fig.colorbar(im, cax=cax, label="Emission in MW/m³")

    def animate(i):
        global time_indicator
        frame = emission_frames[i]/1e6
        vmax = max_abs
        im.set_data(frame)
        im.set_clim(0, vmax)
        ax_profile.clear()
        ax_profile.set_xlabel("rho")
        ax_profile.set_title("Profile")
        ax_profile.plot(profile_frames[i][1], 
                                    profile_frames[i][0]/1000)
        time_indicator.remove()
        time_indicator = ax_meas.axvline(i/(1000/dt_frame), color="black")
        fig.suptitle("Shot {} at t = {} s"
                        .format(shotnumber, round(i*dt_frame/1000,1)))
        plt.tight_layout()

    ani = animation.FuncAnimation(fig, animate, interval=dt_frame*slowdown, 
                                    frames=emission_frames.shape[0])
    print("Animation complete, start writing to file ...")
    ani.save(path, bitrate=-1, dpi=150)
    print("Complete")


def fancy_plot(filtered_measurement, emission, profile, 
                shotnumber, time):
    fig = plt.figure(figsize=(11,5))
    fig.suptitle("Shot {} at t = {} s".format(shotnumber, round(time,1)))
    grid = gridspec.GridSpec(2,3)
    ax_profile = fig.add_subplot(grid[0,1:])
    ax_emission = fig.add_subplot(grid[:,0])
    ax_meas = fig.add_subplot(grid[1,1:])

    ax_meas.set_ylabel("Bolometer signal")
    ax_meas.set_xlabel("time in s")
    ax_profile.set_xlabel("rho")

    plot_emission(emission, fig, ax_emission)
    ax_profile.plot(profile[1], profile[0]/1000)
    ax_meas.plot(np.arange(filtered_measurement.shape[0])/2500, 
                    filtered_measurement)
    ax_meas.axvline(time, color="black")

    plt.tight_layout(rect=[0,0,0.9,1])
    

def visualize_measurements(measurements, fig, ax, title=""):
    full_transfermatrix = np.loadtxt("data/full_transfer_volume.txt")
    
    res = measurements @ full_transfermatrix
    res = res.reshape(85,47) * full_mask

    ax.set_title(title)

    ax.contour(r_coord_full, z_coord_full, full_mask[::-1], 
                levels=[0.5], colors='white')
    img = ax.imshow(res/1000, 
                    extent=[r_coord[0], r_coord[-1], z_coord[0], z_coord[-1]])
    fig.colorbar(img, ax=ax, label="KW/m³")


def plot_local_lengthscale(params, lengthscale_function, fig, ax, 
                            x_point_up=False, size='full'):
    r, z = r_coord_full, z_coord_full
    if size != 'full':
        r,z = r_coord, z_coord
    _, zz = np.meshgrid(r, z)
    local_scale = lengthscale_function(zz[::-1], params, x_point_up)
    img = ax.imshow(local_scale*100, 
                    extent=[r_coord_full[0], r_coord_full[-1], 
                    z_coord_full[0], z_coord_full[-1]])
    ax.contour(r_coord_full, z_coord_full, full_mask[::-1], 
                levels=[0.5], colors='white')
    fig.colorbar(img, ax=ax, label="Lengthscale in cm")
    
    
def compare_emissions(  emissions, 
                        titles,
                        interpol="spline36", 
                        cmap="viridis", 
                        contour_color="white", 
                        adapt_colorscale=True,
                        figsize=(6.8,4.2),
                        custom_colorscale_label=""):

    wspace = 1 * (not adapt_colorscale)
    mask = small_mask

    fig, axes = plt.subplots(1, len(emissions), sharey=adapt_colorscale, 
                    gridspec_kw = {'wspace':wspace, 'hspace':0}, 
                    figsize=figsize, 
                    constrained_layout=False)

    fig.suptitle(titles[0], fontsize=14)

    images = []

    for i, ax in enumerate(axes):

        vmin = 0
        vmax = emissions[0].max()/1000000
        if not adapt_colorscale:
            vmin = emissions[i].min()/1000000
            vmax = emissions[i].max()/1000000

        ax.contour(r_coord, z_coord, mask[::-1], levels=[0.5], colors=contour_color)
        img = ax.imshow(emissions[i]/1000000, interpolation=interpol, vmin=vmin, vmax=vmax,
                extent=[r_coord[0], r_coord[-1], z_coord[0], z_coord[-1]], cmap=cmap)
        images.append(img)
        ax.set_title(titles[i+1])
        #if i == 1:ax.set_xlabel("r in m")
        if i==0 or not adapt_colorscale: ax.set_ylabel("z in m")
        #if i>0: ax.set_yticks([])
        ax.set_xlabel("r in m")
    
    colorscale_label = (custom_colorscale_label if custom_colorscale_label
                        else "Emission in MW/m³")
    
    if adapt_colorscale:
        divider = make_axes_locatable(axes[-1])
        cax = divider.append_axes("right", size="8%", pad=0.05)
        fig.colorbar(images[0], cax=cax, label=colorscale_label)
    else:
        for i, ax in enumerate(axes):
            divider = make_axes_locatable(ax)
            cax = divider.append_axes("right", size="8%", pad=0.05)
            fig.colorbar(images[i], cax=cax, label=colorscale_label)

        plt.tight_layout(rect=[0,0,1,0.1])


# def plot_local_lengthscale_rho(params, lengthscale_function, shot, time,
#                                 fig, ax, size='full'):
#     rho,theta = get_magnetic_coords(shot, time, size)
#     local_scale = lengthscale_function(rho, params)
#     img = ax.imshow(local_scale*100, extent=[r_coord_full[0], r_coord_full[-1], 
#                     z_coord_full[0], z_coord_full[-1]])
#     ax.contour(r_coord_full, z_coord_full, full_mask[::-1], 
#                 levels=[0.5], colors='white')
#     fig.colorbar(img, ax=ax, label="Lengthscale in cm")

# def plot_local_lengthscale_theta(params, lengthscale_function, shot, time,
#                                 fig, ax, size='full'):
#     rho,theta = get_magnetic_coords(shot, time, size)
#     local_scale = lengthscale_function(rho, params)
#     img = ax.imshow(local_scale, extent=[r_coord_full[0], r_coord_full[-1], 
#                     z_coord_full[0], z_coord_full[-1]])
#     ax.contour(r_coord_full, z_coord_full, full_mask[::-1], 
#                 levels=[0.5], colors='white')
#     fig.colorbar(img, ax=ax, label="Lengthscale in radians")
                                        