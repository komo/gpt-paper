\subsection{Results with Real Bolometer Data}\label{section_static}

Using real bolometer data instead of the forward model as input turned out to be 
challenging, as the measurements not only contain statistical noise but also 
systematic errors like drifting channels and measurement spikes for some channels.
Therefore maximizing the marginal log likelihood did not work well and resulted in 
too small length scales for the hyperparameters as the model tried to accommodate for the 
faulty measurements. 

To find good hyperparameters for real bolometer measurements that are not 
affected by systematic errors in some channels, a different approach was taken.
From the 221 available sample emissions the subset of 75 tomograms calculated from 
real measurements was used to find the best parameters for a single sample distribution.
First the bolometer data for the shotnumber and time belonging to the 
training sample is loaded and preprocessed as described in 
\ref{section_preprocessing}. The parameters are then optimized by comparing the 
fitted emission distribution 
\(\vec{E}_{\text{fit}}\) with the existing emission distribution
\(\vec{E}_{\text{sample}}\) with respect to the root mean squared error (RMSE)
and to minimize 
\begin{equation}
f_{\text{opt}}(\vec{\theta}) = \mathrm{RMSE}\left(
    \vec{E}_{\text{fit}}(\vec{\theta}), \vec{E}_{\text{sample}}
\right)
\end{equation}
with respect to the hyperparameters \(\vec{\theta}\).

This resulted in two hyperparameter sets, one for the cartesian and one for the 
combined kernel function for each training sample. In order to obtain a 
parameter set for each of the two kernel functions that works on a broad range 
of scenarios the median of the hyperparameters of all 75 samples is calculated.
The median was chosen over the mean value due to the median being less affected by 
outliers. Table \ref{tab_static_params} lists the parameters for both kernels 
obtained in that way. When looking at the parameters for the combined kernel one 
sees, that \(\sigma_{\text{meq}}\) is one order of magnitude smaller than 
\(\sigma_{\text{val\_l}}\). This is another indicator that the cartesian kernel 
is sufficient and including the magnetic equilibrium kernel might not automatically 
result in better tomograms.

\begin{table}
\centering
\begin{tabular}{l l l}\toprule
    \bfseries Parameters & Cartesian & Combined\\ \midrule
    \(\sigma_{\text{var\_l}}\) [kW/m\textsuperscript{3}] & 145.8 & 137.9\\ 
    \(l_0\)\hspace{.5cm} [cm] & 6.87 & 6.79\\
    \(\delta l\)\hspace{.5cm} [cm] & 25.15 & 26.61 \\
    \(\sigma_{\text{meq}}\) [kW/m\textsuperscript{3}] & - & 11.7 \\
    \(l_{\rho}\)\hspace{.5cm} [\%] & - & 17.29 \\
    \(l_{\theta}\)\hspace{.5cm} [rad] & - & 9.92 \\ 
    \bottomrule
\end{tabular}
\caption{Median of the parameters for the cartesian and combined kernel function 
        after minimizing the RMSE for all 75 samples. The radial 
        length scale \(l_{\rho}\) is given in percent relative to the 
        position of the separatrix and the magnetic equilibrium.}
        \label{tab_static_params}
\end{table}

Figure \ref{img_realdata} shows the comparison between the reconstruction 
via the forward model and the fit calculated from real data with the static cartesian 
parameters listed in table \ref{tab_static_params} for three different 
samples\footnote{Fits for all 75 samples can be found under \url{https://gitlab.mpcdf.mpg.de/komo/gpt-paper/-/tree/master/code/results/fits_static_params/}}. 
While the results are 
qualitatively good in many cases like in those for sample 206 and 168 there are also 
a few cases where there are larger qualitative differences in the emission profile like 
in sample 171. It is also noteworthy that, while the rough structure of the emission 
tomogram are similar in most cases, the amplitude of the fit with real measurements 
especially in the divertor region 
is often too high or to low compared to the sample. 
This shows that fixing the length scales is doable 
without making too much compromises while fixing the \(\sigma_{\text{val\_l}}\) 
parameter, which has an influence on the amplitudes but not so much on the structure 
of the emission tomogram, does not work well when dealing with a broad range of 
different cases. Future work might therefore try to only use fixed length scales 
and try to find a way of estimating \(\sigma_{\text{val\_l}}\) 
directly from the bolometer measurements.


\begin{figure}
    \centering
    \begin{subfigure}{0.8\linewidth}
        \centering
        \includegraphics[width=\linewidth]{realdata206.png}
    \end{subfigure}
    \begin{subfigure}{.49\linewidth}
        \centering
        \includegraphics[width=\linewidth]{realdata169.png}
    \end{subfigure}
    \begin{subfigure}{.49\linewidth}
        \centering
        \includegraphics[width=\linewidth]{realdata171.png}
    \end{subfigure}
    \caption{Fits for three different samples. The left picture shows the already 
    existing tomographic reconstruction for the given shot at the given time. 
    The middle picture the fit via our GPT method from the measurements obtained via 
    the forward model. On the right the fit from the real bolometer measurements is 
    shown with the static parameters for the cartesian kernel.}
    \label{img_realdata}
\end{figure}