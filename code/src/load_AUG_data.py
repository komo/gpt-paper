"""
Loads bolometer, NBI data and magnetic coordinates from AUG
"""

import sys
from pathlib import Path
sys.path.append(str(Path(__file__).parent.absolute()))
sys.path.append('/afs/ipp/aug/ads-diags/common/python/lib')
import numpy as np
import dd
from scipy.stats import norm
from scipy.signal import butter, sosfilt, sosfilt_zi, sosfiltfilt
from pathlib import Path
from constants import STEPS_PER_SECOND_BOLOMETER

DATA_DIR = Path(__file__).parents[1] / "data"


def butter_highpass(highcut: int, fs: float, order:int) -> np.ndarray:
    nyq=0.5+fs
    high = highcut/nyq
    sos = butter(order, high, analog=False, btype="high", output='sos')
    return sos


def butter_highpass_filter(data: np.ndarray, highcut: int, 
                           fs: float, order: int = 3) -> np.ndarray:
    sos = butter_highpass(highcut, fs, order=order)
    return sosfiltfilt(sos, data)


def filter_data(data: np.ndarray, cutoff: float = 30, 
                samplingrate: int = 10000) -> [np.ndarray, np.ndarray]:
    """
    Calculate filtered shot signal
    
    Parameters
    ----------
    data : np.ndarray
        original signal
    cutoff : int, default=30 
        cutoff frequency for filter
    samplingrate : int, default=10000
        samplingrate for filter
    
    Returns
    -------
    highpass_data : np.ndarray
    lowpass_data : np.ndarray
    """

    print("Filtering data")
    highpass_data = np.zeros(data.shape)

    for i in range(data.shape[1]): 
        highpass_data[:, i] = butter_highpass_filter(   data[:, i], 
                                                        cutoff, 
                                                        samplingrate, 
                                                        order=3 )
    lowpass_data = data - highpass_data
    return highpass_data, lowpass_data


def get_NBI_data(shotnumber: int) -> (np.ndarray, np.ndarray):
    """get time basis and power of neutral particle beam"""
    file = dd.shotfile('NIS', shotnumber)
    time_base = file("T-B")
    assert( int((time_base[1]-time_base[0])*1000) == 1 )
    return time_base, file("PNI").data


def get_saved_bolometer_data(samplenumber: int) -> np.ndarray:
    """
    load the real and locally saved bolometer data for the training emissions
    """
    if samplenumber-5 < 0: raise(ValueError("only sample 5-73 available"))
    measurement = np.load(str(DATA_DIR / "real_data_for_5-73.npy"), 
                            allow_pickle=True)[samplenumber-5]
    return measurement[1]


def get_bolometer_data(shotnumber: int) -> np.ndarray:
    "Loads real bolometer measurement for given shotnumber via dd.py"
    
    print("Loading bolometer measurements for "
            + "Shot {} from source".format(shotnumber))
    file = dd.shotfile('BLB', shotnumber)
    powFHC = file("powFHC")
    powFVC = file("powFVC")
    powFDC = file("powFDC")
    powFLX = file("powFLX")
    # only first 4 channels active in FLH
    powFLH = file("powFLH")
    powFHS = file("powFHS")

    data = np.concatenate((powFHC.data,powFVC.data,powFDC.data,
                powFLX.data,powFLH.data[:,:4],powFHS.data), axis=1)[2500:-2500]
    if np.isnan(data).any():
        data[np.isnan(data)] = 0
        print(  "Some measurements contain NaN values. " +
                "Those values will be set to 0"    )

    return data


def get_filtered_bolometer_data(shotnumber: int) -> np.ndarray:
    """returns lowpass filtered bolometer data for given shotnumber"""
    data = get_bolometer_data(shotnumber)
    _, lowpass = filter_data(data)
    return lowpass


def get_data_at_t(data: np.ndarray, time: float) -> np.ndarray:
    """
    takes bolometer data for whole shot as input and returns measurements 
    and given timepoint (in seconds)
    """
    return data[int(time*STEPS_PER_SECOND_BOLOMETER)]