\section{Benchmark of Parameters}

To test the capabilities of the GPT framework with the static parameters 
benchmarks were done with both real and synthetic test data. 
The results obtained for the verification samples are benchmarked by four 
different metrics. The normalized root mean square error (NRMSE) between the fit 
with the parameters and the sample, the structural similarity index SSIM, 
the peak signal to noise ratio (PSNR), which can also be used to quantify the differences 
between two images and is often used in the context of image compression \cite{PSNRjpg},
and the relative deviation in total emission (RDTE), comparing the emission 
in the vessel integrated over the whole volume.

Table \ref{tab_training_verification_overview} shows the number of training 
and verification samples that were randomly drawn for each of the five runs. 
The number of training/verification samples varies as random 
shots are grouped into verification and training shots and the existing samples 
for these shots are then taken as verification/training samples. This is 
necessary to prevent using samples from the same shot for both training and 
verification, as for some shots several samples are available.

First the verification samples are compared to both the result of the GPT 
with the real bolometer measurements corresponding to the sample and by 
applying the forward model to the sample and using the results as input to the 
GPT. For each of the five verification sets the corresponding parameters 
(Table \ref{tab_appendix_param_comb} and \ref{tab_appendix_param_cart}) are used.
The mean results for the different metrics for each of the five verification sets 
are shown in Table \ref{tab_appendix_metrics}. Table \ref{tab_real_results} 
shows the average, best and worst result of the metrics over all five 
verification sets when using real bolometer measurements as input to the GPT, 
while table \ref{tab_forward_results} shows the same with the results from the 
forward model as input.

The results from the computations with the forward model as input are comparable to 
results obtained in previous work \cite{bachelor} with a simpler stationary kernel
with the difference, that there individually optimized parameters were used for each sample to 
calculated the different metrics. This shows, that the static parameters do 
a good job at covering a broad range of emission samples. When using the 
real bolometer measurements as input the metrics are significantly worse. For 
this one has to take into account that the samples were calculated with an algorithm 
that not only took the bolometer measurements as input, but also used a lot of 
human expertise for fine tuning the parameters for each individual sample which 
could take from several minutes to hours. Figure \ref{img_fits_parent_fig} shows 
two sample emissions calculated with the old algorithm compared to the 
corresponding fits with GPT and compares the bolometer measurements with 
the results from the forward model. For the later it is to emphasize that 
both, the results from the forward model for the sample emission as well as the 
results for the emission distribution fitted with real bolometer data show 
significant deviations, well outside of the measurement error for some channels, 
to the actual bolometer measurements. 

When comparing the results for the different parameter sets among each other 
it is noteworthy, that the parameter sets obtained by minimizing the RMSE between 
sample and fitted emission distribution performed overall better in the NRMSE, PSNR and RDTE,
while their SSIM counterparts perform better when taking the SSIM of the fit and 
the sample as metric. This is to be expected, as for these the parameters were obtained 
by optimizing the SSIM in the training process.
Secondly there is no significant difference between the combined kernel and the cartesian kernel.
This could have two possible reasons, one being that the cartesian kernel is good enough, 
as most emission events are strongly localized in the vicinity of the separatrix and the 
divertor. Secondly it could also be, that the optimization of the 
parameters converged to local minima more often for the parameter sets for 
the combined kernel, as more parameters make finding minima/maxima more challenging.
This would negate the positive effects of the more complex kernel function.

Table \ref{tab_synthetic_results} shows the results for the 146 synthetic 
samples, which have been excluded from the training process in \ref{section_static}
as they mostly consist of purely artificial emission distributions, which 
would never occur in real experiments. For those calculations 
the average parameters over the 5 training runs are used (see table \ref{tab_static_params_cartesian}
and \ref{tab_static_params_combined}). Here it is worth mentioning, that the
combined kernel performed slightly better. The reason for this is, that more than 
half of the synthetic samples consisted of emission distributions, that had 
the emission partially or completely smeared out among the magnetic flux 
surfaces. Figure \ref{img_fits_synth_compare} shows the difference between 
the cartesian and the combined kernel for two such synthetic samples.


\begin{table}
\centering
\begin{tabular}{l c c}\toprule
    & training samples & verification samples \\ \midrule
    run 1 & 60 & 15 \\
    run 2 & 59 & 16 \\
    run 3 & 59 & 16 \\
    run 4 & 60 & 15 \\
    run 5 & 61 & 14 \\
    total & 299 & 76 \\
    \bottomrule
\end{tabular}
\caption{Overview of the number of training and verification samples of all the 
            five training runs. For each run shots were randomly selected as 
            training or verification shot and all existing samples belonging 
            to those shots were categorized as training or verification samples.}
\label{tab_training_verification_overview}
\end{table}

\begin{table}
\centering
\begin{subtable}{\linewidth}
    \centering
    \small
    \begin{tabular}{l | c c c | c c c | c c c | c c c}\toprule
        Parameter set & & NRMSE\textdownarrow & & & SSIM\textuparrow & & & 
        PSNR\textuparrow & & & RDTE\textdownarrow\\
        & mean & best & worst & mean & best & worst & mean & best & worst & mean & best & worst \\ \midrule
        cart RMSE & 0.594 & 0.357 & 1.522 & 0.668 & 0.822 & 0.441 & 22.88 & 27.20 & 11.14 & 0.117 & 0.007 & 0.575\\ 
        cart SSIM & 0.624 & 0.404 & 1.598 & 0.691 & 0.844 & 0.449 & 22.42 & 26.72 & 11.12 & 0.120 & 0.003 & 0.573 \\
        comb RMSE & 0.588 & 0.364 & 1.483 & 0.676 & 0.830 & 0.453 & 22.94 & 27.11 & 11.13 & 0.114 & 0.005 & 0.558 \\
        comb SSIM & 0.649 & 0.403 & 1.718 & 0.689 & 0.845 & 0.458 & 22.10 & 26.59 & 11.01 & 0.127 & 0.001 & 0.604 \\
    \bottomrule
    \end{tabular}
    \caption{Real bolometer measurements}
    \label{tab_real_results}
\end{subtable}

\begin{subtable}{\linewidth}
    \centering
    \small
    \begin{tabular}{l | c c c | c c c | c c c | c c c}\toprule
        Parameter set & & NRMSE\textdownarrow & & & SSIM\textuparrow & & & 
        PSNR\textuparrow & & & RDTE\textdownarrow\\
        & mean & best & worst & mean & best & worst & mean & best & worst & mean & best & worst \\ \midrule
        cart RMSE & 0.328 & 0.192 & 0.620 & 0.835 & 0.916 & 0.680 & 27.72 & 31.63 & 22.06 & 0.024 & 0.000 & 0.064 \\ 
        cart SSIM & 0.384 & 0.238 & 0.700 & 0.805 & 0.895 & 0.645 & 26.33 & 29.99 & 21.00 & 0.020 & 0.000 & 0.059 \\
        comb RMSE & 0.336 & 0.196 & 0.629 & 0.829 & 0.913 & 0.680 & 27.51 & 31.24 & 21.94 & 0.025 & 0.001 & 0.072 \\
        comb SSIM & 0.397 & 0.241 & 0.683 & 0.797 & 0.892 & 0.652 & 26.04 & 28.84 & 21.19 & 0.020 & 0.001 & 0.064 \\
    \bottomrule
    \end{tabular}
    \caption{Forward model}
    \label{tab_forward_results}
\end{subtable}

\begin{subtable}{\linewidth}
    \centering
    \small
    \begin{tabular}{l | c c c | c c c | c c c | c c c}\toprule
        Parameter set & & NRMSE\textdownarrow & & & SSIM\textuparrow & & & 
        PSNR\textuparrow & & & RDTE\textdownarrow\\
        & mean & best & worst & mean & best & worst & mean & best & worst & mean & best & worst \\ \midrule
        cart RMSE & 0.341 & 0.101 & 0.949 & 0.747 & 0.962 & 0.247 & 22.63 & 34.22 & 13.61 & 0.043 & 0.001 & 0.244 \\ 
        cart SSIM & 0.375 & 0.123 & 0.956 & 0.711 & 0.924 & 0.245 & 21.67 & 31.00 & 13.20 & 0.046 & 0.000 & 0.343 \\
        comb RMSE & 0.340 & 0.107 & 0.938 & 0.754 & 0.961 & 0.260 & 22.62 & 33.72 & 13.77 & 0.044 & 0.000 & 0.259 \\
        comb SSIM & 0.346 & 0.121 & 0.908 & 0.764 & 0.928 & 0.271 & 22.43 & 30.34 & 13.87 & 0.042 & 0.001 & 0.281 \\
    \bottomrule
    \end{tabular}
    \caption{Synthetic samples}
    \label{tab_synthetic_results}
\end{subtable}
\label{tab_metric_results_overall}
\caption{\textuparrow/\textdownarrow\ indicates that higher/lower values are better. 
            {\bfseries a)} Shows the mean results of the metrics for the verification 
            samples when using real bolometer measurements. {\bfseries b)} Shows 
            the same, but this time the forward model was used for the input 
            measurements. {\bfseries c)} Here the mean for the 146 synthetic 
            samples, which have not been used in the training process, is shown.}
\end{table}

\begin{figure}
    \centering
    \includegraphics[width=.6\linewidth]{fit_36354.png}
    \caption{{\bfseries a)} Emission distribution for shot 36354 at t=6s calculated with 
                the old algorithm. {\bfseries b)} Fitted emission distribution using the 
                combined kernel. Here the actual bolometer measurement 
                was used as input to the GPT. 
                Channels where the measurement (blue line in fig. \ref{img_meas_36354}) is 0, 
                did not record data for the shot, due to various 
                reasons. Such channels are neglected in the GPT calculation.
                {\bfseries c)} Same with the measurements obtained from the 
                forward model for the sample emission.}
    \label{img_fit_36354}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=.6\linewidth]{fit_36236.png}
    \caption{Same as figure \ref{img_fit_36354} for shot 36236 at t=4.6s.}
    \label{img_fit_36236}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=.6\linewidth]{measurement_36354.png}
    \caption{The data points indicate the bolometer measurements for each of 
            the 128 channels together with their measurement error. The orange 
            line shows the measurements according to the forward model for the 
            sample emission (see figure \ref{img_fit_36354}) and the green 
            line for the forward model of emission distribution fitted with 
            real bolometer data. Both the old algorithm (orange line) and the 
            GPT (green line) have deviations well outside the measurement error 
            when comparing results of the forward model with the actual bolometer 
            measurements}
    \label{img_meas_36354}
\end{figure}



\begin{figure}
\centering
\begin{subfigure}{\linewidth}
    \centering
    \includegraphics[width=\linewidth]{fit_synth23.png}
    \caption{}
\end{subfigure}
\begin{subfigure}{\linewidth}
    \centering
    \includegraphics[width=\linewidth]{fit_synth94.png}
    \caption{}
\end{subfigure}
\caption{Two synthetic sample emissions with smeared out emission along 
        flux surfaces. In {\bfseries a)} the result with the combined 
        kernel is smoother compared to the result from the cartesian kernel. 
        In {\bfseries b)} the cartesian kernel is not able to reconstruct the 
        ring in the vessel center, while the combined kernel can. 
        Both examples used the parameters obtained from maximizing the SSIM, as 
        they have a larger \(\sigma_{\mathrm{meq}}\), resulting in a stronger 
        contribution of the magnetic kernel to the combined kernel.}
\label{img_fits_synth_compare}
\end{figure}

