"""
providing static r and z coordinates both for 42x23 and 85x47 pixel resolutions
"""

import numpy as np
from pathlib import Path

data_dir = Path(__file__).parents[1] / "data"

r_coord = np.loadtxt( str(data_dir / "r_coordinates_reduced.txt") )
z_coord = np.loadtxt( str(data_dir / "z_coordinates_reduced.txt") )

r_coord_full = np.loadtxt( str(data_dir / "r_coord.txt") )
z_coord_full = np.loadtxt( str(data_dir / "z_coord.txt") )

rs_small, zs_small = np.meshgrid(r_coord, z_coord)
zs_small = zs_small[::-1]

rs_full, zs_full = np.meshgrid(r_coord_full, z_coord_full)
zs_full = zs_full[::-1]
