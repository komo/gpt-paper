"""calculate radial emission profiles from emission distributions"""

import numpy as np
from scipy.interpolate import interp1d
from static_coords import r_coord, r_coord_full
#from coordinates import get_magnetic_coords


# def get_profile(emission: np.ndarray, 
#                 shotnumber: int, 
#                 time: float, 
#                 segment_count: int = 10, 
#                 interpolate: bool = False) -> (np.ndarray, np.ndarray):
#     """
#     Returns radial emission profile for given emission distribution

#     PARAMETERS
#     ----------
#     emission : np.ndarray
#         emission distribution
#     shotnumber : int
#         shotnumber for obtaining magnetic coordinates
#     time : float
#         time for obtaining magnetic coordinates
#     segment_count : int
#         number of segments that rho is partitioned in. Higher number 
#         leads to higher spatial resolution of profile, but less pixels to 
#         sum over per segment
#     interplolate : bool
#         specifies if spine interpolation is used for profile
    
#     RETURNS
#     -------
#     (np.ndarray, np.ndarray):
#         array with profile, array with rho_coordinates for segments
#     """

#     fullsize = emission.size == 3995

#     rho_coords, _ = get_magnetic_coords(shotnumber, time, fullsize=fullsize)
#     return get_profile_from_coords( emission, 
#                                     rho_coords, 
#                                     segment_count, 
#                                     interpolate)


def get_profile_from_coords(emission: np.ndarray, 
                            rho_coords: np.ndarray,
                            segment_count: int = 10, 
                            interpolate: bool = False) -> np.ndarray:
    """
    same as get_profile but instead of providing shot and time, 
    magnetic coordinates are specified explicitly
    """

    fullsize = emission.size == 3995

    rho_segments = np.array(
        [(i/segment_count, (i+1)/segment_count) for i in range(segment_count)])

    profile = np.zeros(segment_count)

    r = r_coord_full if fullsize else r_coord

    for j, (r_start, r_stop) in enumerate(rho_segments):
        segment_pixels = (r_start <= rho_coords) * (rho_coords < r_stop)
        #print(np.sum(segment_pixels), end=" ")
        profile[j] = (np.sum((r*emission)[segment_pixels]) /
                            np.sum(segment_pixels))

    if interpolate:
        xnew = np.linspace(0,rho_segments[-1,0],100, endpoint=True)
        profile_interpol = interp1d(rho_segments[:,0], profile, kind='cubic')
        return profile_interpol(xnew), xnew
    else:
        return profile, rho_segments[:,0]


def get_profile_time_series(emission_time_series: np.ndarray, 
                            magnetic_coords_series: np.ndarray, 
                            segment_count: int = 10) -> np.ndarray:
    """
    returns array with profiles for every emission distribution in
    emission_time_series
    """

    profiles = []
    for i, emission in enumerate(emission_time_series):
        profile, rho_segments = get_profile_from_coords(emission, 
                            magnetic_coords_series[i,0,:,:],
                            segment_count=segment_count)
        profiles.append([profile, rho_segments])

    return np.array(profiles)