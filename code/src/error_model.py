"""provides functions to get std, variance ... for given measurement"""

# See bachelor thesis for explanation of methodology

import numpy as np
from pathlib import Path

noise_analysis_path = Path(__file__).parents[1] / "data"

background_noise = np.loadtxt(str(noise_analysis_path / 
                "background_noise_complete.txt"))[:,0]
# set to 10000 if background noise is 0 or cov_error wouldn't be invertible
# this is the case for broken/disabled bolometers where no signals were recorded
background_noise[np.isnan(background_noise)] = 10000
signal_dependence = np.loadtxt(str(noise_analysis_path / 
                "noise_dependence_complete.txt"))[:,0]
# negative slopes are set to zero
signal_dependence[signal_dependence < 0] = 0


def get_variance(data: np.ndarray) -> np.ndarray:
    """Returns the variance of a given sensor measurement"""
    return background_noise**2 + signal_dependence * data


def get_std(data: np.ndarray) -> np.ndarray:
    """Returns the standart deviation of a given sensor measurement"""
    return np.sqrt(get_variance(data))


def get_variance_for_fast_mode() -> np.ndarray:
    """Returns variance of 2x background noise as approximation of error"""
    return (2*background_noise)**2

def sample_noise(data: np.ndarray) -> np.ndarray:
    """Samples from data with std. Negative signals are set to 0"""
    std = get_std(data)
    res = np.random.normal(loc=data, scale=std)
    res[res < 0] = 0
    return res