# Loads and stores the real bolometer measurements and magnetic coordinates 
# for all training samples.

from src.load_data import get_synthetic_shot_time, get_filtered_bolometer_data, get_data_at_t
from src.coordinates import get_magnetic_coords
from pathlib import Path
import numpy as np

training_emiss_path = Path("data/training_data/training_emiss")
training_mag_coords = Path("data/training_data/mag_coords")
training_real_meas  = Path("data/training_data/real_measurements")


def save_real_bolometer_data(shotnumber, time, filenumber):
    bolometer_data = get_data_at_t(get_filtered_bolometer_data(shotnumber), time)
    np.save(training_real_meas / "{}.bol.npy".format(filenumber), bolometer_data)

def save_mag_coords(shotnumber, time, filenumber):
    mag_coords = get_magnetic_coords(shotnumber, time)
    np.save(training_mag_coords / "{}.mag.npy".format(filenumber), mag_coords)


for index, path in enumerate(training_emiss_path.glob("*.emiss")):
    print(index, " ", path.parts[-1])
    filenumber = index + 1
    shotnumber, time = get_synthetic_shot_time(filenumber)
    try:
        save_mag_coords(shotnumber, time, filenumber)
        save_real_bolometer_data(shotnumber, time, filenumber)
    except:
        print("Fail for {}".format(filenumber))