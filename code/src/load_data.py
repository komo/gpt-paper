import sys
from pathlib import Path
sys.path.append(str(Path(__file__).parent.absolute()))
import numpy as np
import pandas
from pathlib import Path

DATA_DIR = Path(__file__).parents[1] / "data"


def load_saved_bolometer_data(samplenumber: int) -> np.ndarray:
    """
    load the real and locally saved bolometer data for the training emissions
    """
    path = (DATA_DIR / 
            "training_data/real_measurements/{}.bol.npy".format(samplenumber))
    if not path.is_file():
        raise(ValueError("Saved bolometer data for sample does not exist!"))
    
    return np.load(path)


def downsample_2x(matrix: np.ndarray) -> np.ndarray:
    """Downsampling of last two axes of given numpy array by factor 2"""
    orig_dtype = matrix.dtype
    matrix = matrix.astype(float)
    matrix = (matrix[...,1::2,:] + matrix[...,:-1:2,:])/2.
    matrix = (matrix[...,:,1::2] + matrix[...,:,:-1:2])/2.
    return matrix.astype(orig_dtype)


def load_sample_attributes() -> pandas.DataFrame:
    """
    returns samples with all their attributes (shotnumber, time, ...) as 
    pandas dataframe
    """
    
    shot_list = pandas.read_csv(DATA_DIR / "training_data/shot_mapping.csv", 
                                delimiter=",", usecols=[0,1,3,5,6], 
                                index_col=0, 
                                names=["sample_number", "shotnumber", "time", 
                                        "synthetic", "x_point_up"])

    shot_list["synthetic"] = shot_list["synthetic"] == "Synthetic"
    return shot_list


def load_magnetic_coords(file_number: int) -> np.ndarray:
    """Loads saved magnetic coordinates for samples"""
    path = DATA_DIR / "training_data/mag_coords/{}.mag.npy".format(file_number)
    return np.load(path) 


def get_sample_emission(file_number: int, file_name: str = "{:03d}.emiss", 
                        downscale: bool = True) -> np.ndarray:
    """
    Read synthetic data (emission tomograms) from file.
    Optionally the data is downsampled from 45*83 pixels to 23 * 42 pixels
    """
    
    f = (DATA_DIR / "training_data/training_emiss" / 
            file_name.format(file_number))

    emission = np.zeros((85,47))
    emission[1:-1,1:-1] = np.genfromtxt(str(f), 
                            skip_header=3739).reshape(83,45)[::-1]
    
    if downscale: emission = downsample_2x(emission)

    return emission