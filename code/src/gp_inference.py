"""This is where the magic happens!!!"""

import sys
from pathlib import Path
sys.path.append(str(Path(__file__).parent.absolute()))
import numpy as np
from error_model import get_variance, get_variance_for_fast_mode
from kernels import *
from metrics import integrate_emission
from static_coords import r_coord, z_coord, r_coord_full, z_coord_full

DATA_DIR = Path(__file__).parents[1]/"data"

transfer_mat_full = np.loadtxt(DATA_DIR/"full_transfer_volume.txt")
transfer_mat_small = np.loadtxt(DATA_DIR/"transfermatrix_volume_reduced.txt")

rr, zz = np.meshgrid(r_coord, z_coord)
pts_small = np.vstack((rr.ravel(), zz.ravel()))
rr_full, zz_full = np.meshgrid(r_coord_full, z_coord_full)
pts_full = np.vstack((rr_full.ravel(), zz_full.ravel()))


def get_kernel( kernel_function: str, 
                params: np.ndarray, 
                shotnumber: int = 0, 
                time: float = 0, 
                fullsize: bool = False,
                x_point_up: bool = False,
                magnetic_coords: np.ndarray = np.array([])) -> np.ndarray:
    """
    Return kernel matrix for given coordinates and kernel/covariance function
    
    Parameters
    ----------
    kernel_function : str
        Name of kernel/covariance function (see kernels.py for possible values)
    params : np.ndarray
        Array with parameters for kernel/covariance function. 
        Number of required parameters depends on chosen kernel function
    shotnumber: int
        shotnumber, only required if chosen kernel function includes magnetic
        kernel
    time : float
        time point for shotnumber, only required if chosen kernel function
        includes magnetic kernel   
    fullsize : bool
        returns kernel for 85x47 pixels if true or for 42x23 pixels else 
    x_point_up : bool
        specifies if X-point is up (true) or down (false, default). 
        Only relevant for kernels with non-stationary cartesian component.
    magnetic_coords : np.ndarray
        allows to specify magnetic coordinates to be used explicitly.
    
    Returns
    -------
    kernel_matrix : np.ndarray
    """

    pts = pts_full if fullsize else pts_small
    
    if kernel_function == KERNEL_CARTESIAN:
        return k_cartesian(pts.T, params)
    elif kernel_function == KERNEL_CARTESIAN_VAR_Z:
        return k_cartesian_var_z(pts.T, params, x_point_up)

    else:
        rho, theta = [], []
        if magnetic_coords.any():
            rho, theta = magnetic_coords
        else:
            raise(ValueError("Please specify magnetic coordinates when using " 
                               + "kernels with magnetic components!"))
            #if not shotnumber or not time:
            #    raise(ValueError("Please specify shotnumber and time when"
            #                + "using kernels with magnetic components!"))
            #rho,theta = get_magnetic_coords(shotnumber, time, fullsize)
        
        pts = np.append(pts, np.atleast_2d(rho.ravel()), axis=0)
        pts = np.append(pts, np.atleast_2d(theta.ravel()), axis=0)

        if kernel_function == KERNEL_MAGNETIC:
            return k_magnetic(pts.T, params)
        elif kernel_function == KERNEL_COMBINED:
            return k_combined(pts.T, params)
        elif kernel_function == KERNEL_COMBINED_VAR_Z:
            return k_combined_var_z(pts.T, params, x_point_up)

        # Experimental kernels
        # elif kernel_function == "magnetic_var_rho":
        #     return k_magnetic_var_rho(pts.T, pts.T, params)
        # elif kernel_function == "magnetic_var_theta":
        #     return k_magnetic_var_theta(pts.T, pts.T, params)
        # elif kernel_function == "magnetic_var_both":
        #     return k_magnetic_var_rhotheta(pts.T, pts.T, params)
        
        # elif kernel_function == "combined_var_both_rho":
        #     return k_combined_var_zrho(pts.T, pts.T, params,
        #                                             x_point_up)
        # elif kernel_function == "combined_var_both_both":
        #     return k_combined_var_everything(pts.T, pts.T, params, x_point_up)
        else:
            raise(ValueError("Kernel function '{}' not defined!"
                    .format(kernel_function)))

        
def get_transfermat(fullsize: bool, active_channels: np.ndarray, 
                    measurements: np.ndarray) -> np.ndarray:
    """
    Returns transfermatrix

    Parameters
    ----------
    fullsize : bool
        returns transfer_matrix for 85x47 pixels if true or 
        for 42x23 pixels else 
    active_channels : np.ndarray
        boolean array to specify which channels should be used. 
        For channels that are set to false, the respective row in the 
        transfermatrix is set to 0
    measurements : np.ndarray
        used to filter out channels where the bolometer data is < 0 or = 0
    
    Returns
    -------
    np.ndarray :
        transfer_matrix
    """
    channels = active_channels
    T = transfer_mat_full.copy() if fullsize else transfer_mat_small.copy()
    channels = channels * (measurements > 0)
    T[np.logical_not(channels)] = 0
    return T


def performGP(  measurements: np.ndarray, 
                params: np.ndarray, 
                active_channels: np.ndarray = np.ones(128, dtype=bool),
                kernel_function: str = KERNEL_CARTESIAN_VAR_Z, 
                variance: np.ndarray = np.array([]),
                fullsize: bool = False,
                format: str = "image" ,
                shotnumber: int = 0, 
                time: float = 0,
                restrict_to_vessel: bool = True,
                remove_negative_emission: bool = True,
                x_point_up: bool = False,
                magnetic_coords: np.ndarray = np.array([]),
                ) -> np.ndarray: # Tuple of two ndarrays
    """
    This is where the magic happens!!!
    
    Parameters
    ----------
    measurements : np.ndarray
        (Bolometer) measurements
    params : np.ndarray
        Array with parameters for kernel/covariance function. 
        Number of required parameters depends on chosen kernel function
    active_channels : np.ndarray
        boolean array to specify which channels should be used.
        On default all are used.
    kernel_function : str
        Name of kernel/covariance function (see kernels.py for possible values)
    variance : np.ndarray
        variance of measurements
    fullsize : bool
        return emissionwith 85x47 pixels if true or with 42x23 pixels else
    format : str
        "image": (default) return emission distribution as 2D-array
        "raw" return emission distribution as 1D-vector
    shotnumber: int
        shotnumber, only required if chosen kernel function includes magnetic
        kernel
    time : float
        time point for shotnumber, only required if chosen kernel function
        includes magnetic kernel
    restrict_to_vessel : bool
        if true and format = "image" the returned emission distribution 
        is multiplied with the vessel shape, to make all values outside to 
        vessel 0 
    remove_negative_emissions : bool
        if true channels only channels with measurements > 0 are used
    x_point_up : bool
        specifies if X-point is up (true) or down (false, default). 
        Only relevant for kernels with non-stationary cartesian component.
    magnetic_coords : np.ndarray
        allows to specify magnetic coordinates to be used explicitly.
    
    Returns
    -------
    np.ndarray, np.ndarray : 
        fitted emission distribution, diagonal of posterior covariance
    """

    #import time as timer
    #start = timer.perf_counter()
    mean = np.zeros(3995) if fullsize else np.zeros(966)

    T = get_transfermat(fullsize, active_channels, measurements)
    
    if not variance:
        variance = get_variance(measurements)*.5

    sigma_d = np.diag(variance)

    
    sigma_d_inv = np.linalg.inv(sigma_d)

    kernel = get_kernel(kernel_function, 
                        params, 
                        shotnumber, 
                        time, 
                        fullsize, 
                        x_point_up,
                        magnetic_coords)
    
    print(np.linalg.cond(kernel))
    
    
    
    kernel_inv = np.linalg.inv(kernel)
    
    sigma_E_post = np.linalg.inv(T.transpose() @ sigma_d_inv @ T + kernel_inv)

    fit = mean + (sigma_E_post @ T.transpose() @ sigma_d_inv
                    @ (measurements - T @ mean))

    postCov = sigma_E_post.diagonal()

    

    dim = (85,47) if fullsize else (42,23)

    if format == "image":
        mask = (np.loadtxt("data/mask_fullres.txt") if fullsize 
                            else np.loadtxt("data/mask.txt"))
        fit = fit.reshape(dim[0], dim[1])
        postCov = postCov.reshape(dim[0], dim[1])
        if restrict_to_vessel:
            postCov = postCov * mask
            fit = fit * mask
    elif format != "raw": 
        raise ValueError("{} is not a valid return format!".format(format))

    if remove_negative_emission:
        total_emiss_old = integrate_emission(fit)
        fit[fit<0] = 0
        total_emiss_new = integrate_emission(fit)
        if total_emiss_old < 0:
            print("Warning: Integrated emission is negative!")
            print("Stop renormalizing emission")
        else:
            fit = fit * (total_emiss_old/total_emiss_new.sum())

    #print(" ", timer.perf_counter()-start)  
    return fit, postCov


def calculate_timeseries(   bolo_data_frames: np.ndarray, params: np.ndarray,
                            mag_coords_frames: np.ndarray = [], 
                            kernel_function: str = KERNEL_COMBINED_VAR_Z,
                            x_point_up: bool = False) -> np.ndarray:
    """
    Calculates emission distribution for a whole time series

    Parameters
    ----------
    bolo_data_frames : np.ndarray
        array containing arrays of measurements for all time points
    params : np.ndarray
        Array with parameters for kernel/covariance function. 
        Number of required parameters depends on chosen kernel function
    mag_coords_frames : np.ndarray
        array containing arrays of magnetic coordinates for all time points
    kernel_function : str
        Name of kernel/covariance function (see kernels.py for possible values)
    x_point_up : bool
        specifies if X-point is up (true) or down (false, default). 
        Only relevant for kernels with non-stationary cartesian component.
    
    Returns
    -------
    np.ndarray :
        array containing arrays for all fitted emission distributions
    """
    
    if ((kernel_function is not KERNEL_CARTESIAN and kernel_function 
            is not KERNEL_CARTESIAN_VAR_Z) and not len(mag_coords_frames)):
        print("please provide magnetic coordinates for each frame \
                when using kernels with magnetic components")

    fits = []
    mag_coords_frame = []
    for frame, frame_measurement in enumerate(bolo_data_frames):
        if len(mag_coords_frames):
            mag_coords_frame = mag_coords_frames[frame]
        print("\rPerforming GPT for frame {} of {}"
                .format(frame, bolo_data_frames.shape[0]), end="")
        fit, _ = performGP( frame_measurement, 
                            params, 
                            kernel_function = kernel_function,
                            magnetic_coords=mag_coords_frame,
                            x_point_up=x_point_up)
        if np.isnan(fit).any():
            print(" WARNING: nan value(s) at frame {}".format(frame))
        fits.append(fit)
    
    print("")
    return np.array(fits)


def calculate_timeseries_fast(  bolo_data_frames: np.ndarray, 
                                params: np.ndarray,
                                x_point_up: bool = False) -> np.ndarray:
    """
    Calculates emission distribution for a whole time series fast by only
    calculating the inversions once. This only works for the cartesian kernel 
    and with a constant error for each channel, as error and kernel that 
    changes over time would require inversion for each time point.

    Parameters
    ----------
    bolo_data_frames : np.ndarray
        array containing arrays of measurements for all time points
    params : np.ndarray
        Array with parameters for kernel/covariance function. 
        Number of required parameters depends on chosen kernel function
    mag_coords_frames : np.ndarray
        array containing arrays of magnetic coordinates for all time points
    kernel_function : str
        Name of kernel/covariance function (see kernels.py for possible values)
    x_point_up : bool
        specifies if X-point is up (true) or down (false, default). 
        Only relevant for kernels with non-stationary cartesian component.
    
    Returns
    -------
    np.ndarray :
        array containing arrays for all fitted emission distributions
    """
    print("start fast time series")
    
    mask = np.loadtxt("data/mask.txt")
    
    T = get_transfermat(False, np.ones(128, dtype=bool), bolo_data_frames[0])

    variance = get_variance_for_fast_mode()
    sigma_d = np.diag(variance)
    sigma_d_inv = np.linalg.inv(sigma_d)

    kernel = get_kernel(KERNEL_CARTESIAN_VAR_Z, params, x_point_up=x_point_up)
    kernel_inv = np.linalg.inv(kernel)
    
    sigma_E_post = np.linalg.inv(T.transpose() @ sigma_d_inv @ T + kernel_inv)

    fits = []
    warning = False
    print("posterior covariance calculated, starting with fits:")
    import time as timer
    start = timer.perf_counter()
    for frame, frame_measurement in enumerate(bolo_data_frames):
        
        fit = (sigma_E_post @ T.transpose() @ sigma_d_inv @ (frame_measurement))
        
        if np.isnan(fit).any():
            print(" WARNING: nan value(s) at frame {}".format(frame))

        
        fit = fit.reshape(42, 23)
        fit = fit * mask

        old_sum = fit.sum()
        fit[fit<0] = 0
        if old_sum < 0:
            warning = True
        else:
            fit = fit * (old_sum/fit.sum())
        
        fits.append(fit)
        
    print(" ", timer.perf_counter()-start)
    if warning:
            print("Warning: Integrated emission was negative for at least 1 time point!")
            print("Stopped renormalizing emission")   
    print("finished")
    return np.array(fits)


def neg_log_likelihood( measurements: np.ndarray, 
                        params: np.ndarray,
                        active_channels: np.ndarray = np.ones(128, dtype=bool), 
                        kernel_function: str = KERNEL_CARTESIAN_VAR_Z,
                        fullsize: bool = False,
                        shotnumber:int = 0,
                        time: float = 0,
                        x_point_up: bool = False,
                        magnetic_coords: np.ndarray = np.array([])) -> float:
    """
    calculate negative log likelihood
    
    Parameters
    ----------
    measurements : np.ndarray
        (Bolometer) measurements
    params : np.ndarray
        Array with parameters for kernel/covariance function. 
        Number of required parameters depends on chosen kernel function
    active_channels : np.ndarray
        boolean array to specify which channels should be used.
        On default all are used.
    kernel_function : str
        Name of kernel/covariance function (see kernels.py for possible values)
    fullsize : bool
        return emissionwith 85x47 pixels if true or with 42x23 pixels else
    shotnumber: int
        shotnumber, only required if chosen kernel function includes magnetic
        kernel
    time : float
        time point for shotnumber, only required if chosen kernel function
        includes magnetic kernel
    x_point_up : bool
        specifies if X-point is up (true) or down (false, default). 
        Only relevant for kernels with non-stationary cartesian component.
    magnetic_coords : np.ndarray
        allows to specify magnetic coordinates to be used explicitly
    
    Returns
    -------
    float : 
        negative log likelihood
    """
   
    T = get_transfermat(fullsize, active_channels, measurements)

    sigma_d = np.diag(get_variance(measurements))
    
    kernel = get_kernel(kernel_function,
                        params,  
                        shotnumber, 
                        time,
                        fullsize, 
                        x_point_up,
                        magnetic_coords )

    intermediate = T @ kernel @ T.transpose() + sigma_d
    _, log_determinante = np.linalg.slogdet(intermediate)
    
    #print(np.linalg.cond(intermediate))

    mean = np.zeros(3995) if fullsize else np.zeros(966)

    res = (0.5*log_determinante + np.abs(
            0.5*((measurements - T @ mean).T 
                    @ np.linalg.inv(intermediate) 
                    @ (measurements - T @ mean))))

    return res
