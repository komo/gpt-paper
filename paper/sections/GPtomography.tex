\section{Gaussian Process Tomography (GPT)}

Mathematically a single bolometer channel performs an integration over the 
continuous emissivity function \(E(\vec{r})\), describing the emission 
distribution within the vessel.
\begin{equation}\label{kap3_eq_sightline_integral}
    P_i = \int S_i(\vec{r})E(\vec{r})\mathrm{d^3r} + \varepsilon_i
\end{equation}
Here \(S_i\) is the response function for the \(i\)-th bolometer channel that describes 
the viewing cone or how strong a infinitesimal volume element contributes to the 
channel. \(P_i\) is the measured power
of the \(i\)-th bolometer channel. By switching to a discrete emission distribution
over \(n\) pixels the measurement process for all \(m\) channels can be 
written as a matrix multiplication, that will be called forward model from here on,
\begin{equation}\label{eq_forward_model}
    \vec{P} = \mathbf{S} \cdot \vec{E} + \vec{\varepsilon}
\end{equation} 
where \(\vec{\varepsilon}\) is the measurement error and 
\(\mathbf{S}\) is the transfer matrix of size \(m\times n\), mapping how much each of 
the \(n\) pixels contributes to the sight cones of the \(m\) bolometer channels. A simple 
inversion of the equation above in order to obtain the emission distribution 
for a given measurement is not feasible as there are more unknown pixels than 
known measurements making the problem ill-conditioned. 
To solve this problem we assume both our emissivity and 
our measurements to be described by a multivariate normal distribution \(\mathcal{N}\)
\begin{equation}
    \vec{P}\ \sim\ \mathcal{N}(\vec{\mu}_P, \mathbf{\Sigma}_P),\quad 
    \vec{E}\ \sim\ \mathcal{N}(\vec{\mu}_E, \mathbf{\Sigma}_E)
\end{equation}
with a respective mean \(\vec{\mu}\) and covariance \(\mathbf{\Sigma}\).
As multivariate normal distributions the probability density 
functions for \(\vec{P}\) and \(\vec{E}\) are given by
\begin{align}
    \begin{split}\label{eq_p_E}
        p(&\vec{E}) \propto
        \exp\left[-\frac{1}{2} (\vec{E}-\vec{\mu}_E)^T\mathbf{\Sigma}_E^{-1}
        (\vec{E}-\vec{\mu}_E) \right]
    \end{split}\\
    \begin{split}\label{eq_p_d_given_E}
        p(\vec{P}|&\vec{E}) \propto
        \exp\left[-\frac{1}{2} (\vec{P}-\vec{\mu}_{P|E})^T
        \mathbf{\Sigma}_P^{-1}(\vec{P}-\vec{\mu}_{P|E}) \right]
    \end{split}
\end{align}
By applying Bayes’ Theorem we can express a conditional probability 
distribution of \(\vec{E}\) for a given \(\vec{P}\)
\begin{equation}\label{eq_p_bayes}
    p(\vec{E}|\vec{P}) \propto\ p(\vec{P}|\vec{E})p(\vec{E})
\end{equation}
which as a product of two Gaussians is a Gaussian distribution itself.
\begin{equation}\label{eq_E_post}
    p(\vec{E}|\vec{P}) \propto 
    \exp\left[-\frac{1}{2} (\vec{E}-\vec{\mu}_E^{post})^T
    [\mathbf{\Sigma}_E^{post}]^{-1}(\vec{E}-\vec{\mu}_E^{post})\right]
\end{equation}
The mean of the measurements for a given emission distribution \(\vec{\mu}_{P|E}\) 
can be expressed via the forward model 
(Eq. \ref{eq_forward_model})
\begin{equation}\label{eq_d_prior}
    \vec{\mu}_{P|E} = \mathbf{S} \cdot \vec{E}
\end{equation}
By plugging the prior likelihood distribution \ref{eq_p_E}, \ref{eq_p_d_given_E} and 
Eq. \ref{eq_d_prior} into
Eq. \ref{eq_p_bayes} we can obtain the posterior mean and covariance for 
the posterior distribution given by Eq. \ref{eq_E_post} under the assumption 
that the prior mean \(\vec{\mu}_E\) is 0.
\begin{align}
    \begin{split}\label{eq_mean_post}
        &\vec{\mu}_E^{post} = \mathbf{\Sigma}_E^{post}
        \mathbf{S}^T\mathbf{\Sigma}_P^{-1}\vec{P}
    \end{split}\\
    \begin{split}\label{eq_mean_cov}
        &\mathbf{\Sigma}_E^{post} = (\mathbf{\Sigma}_E^{-1} + 
        \mathbf{S}^T\mathbf{\Sigma}_P^{-1}\mathbf{S})^{-1}
    \end{split}
\end{align}
A more thorough derivation is given in \cite{Bishop}.

In order to get the most likely emission distribution for a given bolometer 
measurement one has to construct a viable covariance/kernel matrix 
\(\mathbf{\Sigma}_E\) and use Eq. \ref{eq_mean_post}. The individual bolometer 
channels are assumed to be unrelated, because they are not physically coupled and 
local events can appear on single channels. Hence the covariance matrix for the 
measurements
\(\mathbf{\Sigma}_P\) is given by a diagonal matrix with 
\(\Sigma_{P}^{ii} = \sigma_i^2\), where \(\sigma_i^2\) is the variance of the i-th
channel given by \(\sigma_i^2=\langle\varepsilon_i^2\rangle\). A complete treatment of 
Gaussian process regression and various kernel functions can be found in 
\cite{Bishop, Rasmussen06}.

\subsection{Non-Stationary Kernels}
In previous work \cite{bachelor} a simple stationary squared exponential 
kernel function was used to construct the kernel matrix \(\mathbf{\Sigma}_E\)
for the prior distribution. 
\begin{equation}\label{eq_stat_kernel}
    \Sigma_E^{ij} = k_{SE}(\vec{r}_i, \vec{r}_j) = \sigma_E^2\exp
    \left(-\frac{(\vec{r}_i-\vec{r}_j)^2}{2l^2}\right)
\end{equation}
Where \(\vec{r}_i\) corresponds to the cartesian coordinates of the \(i\)-th pixel.
The assumption was that pixels are strongly correlated with neighboring pixels 
and less correlated with pixels further away. Both \(\sigma_E\) and \(l\) are 
hyperparameters describing variance of the prior emission distribution and 
how fast the correlation drops off with increasing distance between pixels.
While this simple kernel function was successful at reconstructing training 
samples of emission distributions where the input measurement \(\vec{P}\)
was obtained by applying the forward model to the training samples, it failed 
at producing meaningful results for real bolometer data as input. 

By using
a variable length scale \(l\) for the covariance function it is possible to 
get a more refined kernel, describing varying local behavior of the 
emission distribution. While the previous kernel function was stationary in the 
sense that it only depended on the distance between two pixels and not 
their absolute coordinates, this is not the case for a kernel function with locally 
varying hyperparameters. A general non-stationary squared exponential kernel 
function with variable length scale would look like the following 
\begin{equation}\label{eq_non_stat_kernel}
    k_{\text{var\_l}}(\vec{r}_i, \vec{r}_j) = \sigma_{\mathrm{var\_l}}^2\exp
    \left(-\frac{(\vec{r}_i-\vec{r}_j)^2}{l(\vec{r}_i)^2 + l(\vec{r}_j)^2}\right)
\end{equation}

In our case it proved to be useful to have lower length scales in the 
region of the divertor where emission events are more localized 
than in the main part of the vessel. To encode this behavior into the kernel 
function we chose a hyperbolic tangent along the z-axis for the length scale 
function.
\begin{equation}
    l(\vec{r}) = l(z) = l_0 + \delta l\left[1+\tanh\left(\frac{z-z_0}
    {\delta z}\right)\right]
\end{equation}
Here \(l_0\) and \(\delta l\) are hyperparameters describing the length scale in the 
divertor region and the value that is added to the length scale in the main 
region of the vessel. \(z_0\) and \(\delta z\) describe the position and slope 
of the transition between the two length scales. For shots with the X point 
in the lower vessel part \(z_0\) was fixed to -75 cm and \(\delta z\) was 
chosen to be 20 cm. In cases were the X point is in the upper vessel part
both \(z_0\) and \(\delta z\) are multiplied with -1. Figure 
\ref{img_local_lengthscale} shows an example for the local length scale.
\begin{figure}
    \centering
    \includegraphics[width=0.7\linewidth]{local_lengthscale.png}
    \caption{Local length scale \(l(z)\) in cm for both scenarios with 
    the X point in the lower or upper vessel region. \(l_0\) and \(\delta l\)
    were take from the static parameters found in section 
    \ref{section_static}. The white contour indicates the vessel boundaries.}
    \label{img_local_lengthscale}
\end{figure}

\subsection{Magnetic Equilibrium Information}

The emissivity in the plasma is known to correlate with the magnetic flux 
surfaces, meaning that it varies more perpendicular to them, than parallel 
to them. To encode such behavior into a squared exponential kernel, the 
distance of two pixels has to be calculated with respect to their magnetic 
coordinates \(\rho\) and \(\theta\). A non stationary squared exponential kernel 
would then take the following form
\begin{equation}\label{eq_mag_kernel}
    k_{\text{meq}}(\rho_i, \theta_i, \rho_j, \theta_j) = 
    \sigma_{\mathrm{meq}}^2\exp\left[-\left(\frac{(\rho_i-\rho_j)^2}{2l_{\rho}^2} + 
    \frac{(\theta_i-\theta_j)^2}{2l_{\theta}^2}\right)\right]
\end{equation}
Here \(l_{\rho}\) and \(l_{\theta}\) are the length scales along the poloidal 
coordinates, where \(\rho\) describes the position perpendicular to the magnetic 
flux and \(\theta\) is the poloidal angle.
Figure \ref{img_magnetic_distance} depicts the distance with respect 
to \(\rho\) and \(\theta\) for an example.
\begin{figure}
    \centering
    \includegraphics[width=0.8\linewidth]{magnetic_distance.png}
    \caption{Distance perpendicular and parallel to magnetic flux surfaces 
    for a point with coordinates \((\rho_i, \theta_i) = (0.7, 0)\). \(\rho=0\) 
    corresponds to the magnetic center and \(\rho=1\) corresponds to the separatrix.
    \(\theta\) is given in radians. The magnetic flux surfaces were taken 
    from shot 36173 at \(t=6.0\) seconds.}
    \label{img_magnetic_distance}
\end{figure}

\subsection{Implementation for AUG}

For this paper 128 bolometer channels grouped into 6 different 
cameras were used to calculate emission distributions for ASDEX Upgrade. 
A thorough overview of the bolometer diagnostic at ASDEX Upgrade is given in 
\cite{BolometerDiplom, BolometerPHD}.
A transfer matrix \(\mathbf{S}\), 
describing the sight cones of the 128 
bolometer channels for an emission distribution of 85x47 pixels was developed 
in previous work for AUG \cite{pierresPaper}. Recently additional bolometer 
channels were installed, but since the transfer matrix was developed for the 
campaign ranging from shot 35001 to 36794 where those channels were not available 
they are neglected in this paper. Figure \ref{img_bolometer_channels} shows the 
geometry of the bolometer channels at AUG, while figure \ref{img_transfer_channel10} 
shows how the 30th channel of the FHC camera is modeled by the transfer matrix.
\begin{figure}
\centering
\begin{subfigure}{.3\linewidth}
    \centering
    \includegraphics[width=\linewidth]{bolometer_channels.png}
    \subcaption{}
    \label{img_bolometer_channels}
\end{subfigure}
\begin{subfigure}{.35\linewidth}
    \centering
    \includegraphics[width=\linewidth]{transfer_channel30.png}
    \subcaption{}
    \label{img_transfer_channel10}
\end{subfigure}
\caption{ \textbf{a)} Geometry of all 128 bolometer channels covered by the 
transfer matrix used for this paper. \textbf{b)} Channel 30 of the 
FHC camera depicted via the transfer matrix.}
\end{figure}

The GPT used in this paper requires the explicit inversion of the kernel matrix
\(\mathbf{\Sigma}_E\) (See Eq. \ref{eq_mean_post} and \ref{eq_mean_cov}). 
As the matrix inversion of a \(n\times n \) matrix
via a LU- or Cholesky-decomposition is known to scale with 
\(\mathcal{O}(n^3)\) it was decided to work with a lower resolution of 
42x23 pixels for faster calculation. The transfer matrix was scaled down 
by a factor of 2 to achieve this. Comparison with results obtained by 
performing the calculation with full and half resolution showed no significant
differences once both results were scaled up to higher resolutions via 
spline interpolation. Therefore using the lower resolution does not lose 
any information of the underlying emission distribution as long as the 
length scales for the GPT are not smaller than the size of one pixel.

The GPT algorithm is implemented in Python using the Numpy package. One 
full computation of the posterior mean and covariance from a given bolometer 
measurement including the calculation of the kernel matrix and the 
matrix inversion takes around 180 ms on a midrange PC with an AMD R5 1600 
processor and 120 ms on a laptop with an AMD R7 4800H processor.