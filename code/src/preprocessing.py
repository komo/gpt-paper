"""functions for checking if bolometer measurements are valid"""

import sys
from pathlib import Path
sys.path.append(str(Path(__file__).parent.absolute()))
import numpy as np
import pandas as pd
from scipy import interpolate
from scipy.stats import norm
from constants import STEPS_PER_SECOND_BOLOMETER, STEPS_PER_SECOND_NBI
from load_AUG_data import filter_data, get_NBI_data
from error_model import get_std


def check_channels(bolometer_signal: np.ndarray, shotnumber: int) -> np.ndarray:
    """
    Loops over all channels in bolometer_signal and returns boolean array 
    with active channels
    """
    
    active_channels = np.ones(bolometer_signal.shape[1], dtype=bool)

    data_NBI = get_NBI_data(shotnumber)

    noise_check_results = check_noise_level(bolometer_signal)

    for channel in range(bolometer_signal.shape[1]):
        
        if check_integrated_signal(bolometer_signal[:,channel], data_NBI):
            print("deactivate channel {} due to too high integrated signal"
                    .format(channel))
            active_channels[channel] = False

        if check_signal_values(bolometer_signal[:,channel]):
            print("deactivate channel {} due to too high signal"
                    .format(channel))
            active_channels[channel] = False

        if check_if_more_negative(bolometer_signal[:,channel]):
            print("deactivate channel {} due to too much negative signal"
                    .format(channel))
            active_channels[channel] = False

        if check_if_straight_line(bolometer_signal[:,channel]):
            print("deactivate channel {}, interval with no noise detected"
                    .format(channel))
            active_channels[channel] = False

        if noise_check_results[channel]:
            print("deactivate channel {} due to too high noise"
                    .format(channel))
            active_channels[channel] = False

    return active_channels



def check_integrated_signal(bolometer_signal_channel, data_NBI):
    """
    checks if integrated signal is too high compared to NBI power,
    implemented according to Pierre David
    """

    power_NBI = data_NBI[1]
    time_NBI = data_NBI[0]
    max_time = round(time_NBI[-1],2)
    
    x_old = np.arange(0,max_time, 1/STEPS_PER_SECOND_NBI)
    x_old[-1] += 0.001 # otherwise x_new[-1] outside interpolation range
    x_new = np.arange(0,max_time, 1/STEPS_PER_SECOND_BOLOMETER)

    signal = bolometer_signal_channel[0:x_new.size]

    f = interpolate.interp1d(x_old, power_NBI)

    power_NBI_interpol = f(x_new)

    mask = (power_NBI_interpol > 0)
    #import matplotlib.pyplot as plt
    #plt.plot(power_NBI_interpol)

    # sum(dt_bolo.*abs(boloSig./P_NBI')) > 35e6
    result = np.sum( 1/STEPS_PER_SECOND_BOLOMETER *
                        np.abs( signal[mask]/power_NBI_interpol[mask] ) )
    return result > 35e6


def check_signal_values(bolometer_signal_channel):
    """
    checks if signal values are too high, implemented according to Pierre David
    """
    result = np.sum( 1/STEPS_PER_SECOND_BOLOMETER * 
                    ( np.abs( bolometer_signal_channel ) > 30e6 ) )
    return result > 0.005


def check_if_more_negative(bolometer_signal_channel):
    """checks if channel has to much negative signal"""
    positive_avg = bolometer_signal_channel[bolometer_signal_channel>0].mean()
    negative_avg = bolometer_signal_channel[bolometer_signal_channel<0].mean()
    return np.abs(negative_avg)>positive_avg


def check_noise_level(bolometer_signal):
    """
    returns boolean list with all channels were the noise is much higher
    than expected. To do this, the noise of the channels is calculated 
    by applying a highpass filter and calculating the std of the highpass data.
    This noise is then compared to the noise that would be expected from 
    the error_model.
    """
    # calculate avg noise of shot
    highpass, _ = filter_data(bolometer_signal)
    avg_noises = []
    for channel in range(bolometer_signal.shape[1]):
        _, avg_noise = norm.fit(highpass[:,channel])
        avg_noises.append(avg_noise)
    avg_noises = np.array(avg_noises)

    avg_bolo_levels = bolometer_signal.mean(axis=0)
    
    # max allowed noise is 5 times the noise that would be expected from 
    # the error_model
    max_error = 5*get_std(avg_bolo_levels)

    return avg_noises > max_error


def check_if_straight_line(bolometer_signal_channel):
    """
    checks if data contains a straight line with no noise. Triggers if straight
    line in longer than 200ms
    """
    # Calculate rolling std over 200ms
    window = int(0.2*STEPS_PER_SECOND_BOLOMETER)
    data = pd.Series(bolometer_signal_channel)
    rolling_std = data.rolling(window).std()
    # rolling std never goes fully to 0
    return np.any(rolling_std<0.1)


