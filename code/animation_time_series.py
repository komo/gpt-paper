"""generate animations for specified shot and time interval"""

import numpy as np
from src.gp_inference import performGP, calculate_timeseries, calculate_timeseries_fast
from src.plot_helper import save_time_series
from pathlib import Path
from src.constants import PARAMS_STATIC_CART, STEPS_PER_SECOND_BOLOMETER
from src.kernels import KERNEL_CARTESIAN_VAR_Z, KERNEL_CARTESIAN

def generate_time_series_animation( shot: int, 
                                    t_start: float, 
                                    t_stop: float,
                                    dt_frame: int = 20,
                                    params: np.ndarray = PARAMS_STATIC_CART,
                                    kernel_function: str = KERNEL_CARTESIAN_VAR_Z,
                                    x_point_up: bool = False,
                                    custom_save_path: str = None,
                                    save_options: dict = {},
                                    fast_mode: bool = False):
    """
    generate animation for specified shot and time interval. make sure 
    to place timeseries for bolometer measurements (and magnetic coordinates)
    into data/time_series.

    PARAMETERS
    ----------
    shot : int
        shotnumber to be used
    t_start : float
        start time in seconds
    t_stop : float
        stop time in seconds
    dt_frame: int
        frame time in milliseconds
    params : np.ndarray
        parameters for chosen kernel function
    kernel_function : str
        specifies kernel function to be used
    x_point_up : bool
        set to true if shot as an upper X-point
    custom_save_path : str
        specifies filepath for amimations. On default they are saved in 
        results/time_series
    save_options : dict
        dictionary with the following required entries:
            static_scale: boolean or boolean list, specifies if the colorscale 
                is kept fixed
            slowdown: int or int list, specifies the speed of the animation.
                if 2 for example 2 seconds of animation are 1 second real time
            title: string or string list, specifies the titles of the animated
                plots
        optionally one might also define:
            file_name: custom file_name(s) for saved animations
        if several values are given (must be the same number for all fields)
        then several animations for the same shot are saved with the specified
        save options
    fast_mode : bool
        use fast calculation (only cartesian kernel) or not
    """
    
    save_options_check = ("static_scale" in save_options
                          and "slowdown" in save_options
                          and "title" in save_options)
    if save_options and not save_options_check:
        raise TypeError("save_options must contain the following keys:\n"
                        + "static_scale, slowdown, title, file_name"
                        + "(optional)\n")

    
    # Define paths for loading and saving data
    data_dir = Path("data/time_series")
    result_dir = (Path(custom_save_path) if custom_save_path 
                  else Path("results/time_series"))

    # Load bolometer measurements
    print("Loading bolometer measurements for shot {}".format(shot))
    measurement_path = (data_dir / "shot{}bolo_data_{}_{}_{}.npy"
                        .format(shot, t_start, t_stop, dt_frame))
    frame_measurements = [] 
    if measurement_path.is_file():
        frame_measurements = np.load(measurement_path)
    else: raise(ValueError("provide bolometer data"))

    # load magnetic coordinates
    mag_coords_required = not (kernel_function == KERNEL_CARTESIAN_VAR_Z
                                or kernel_function == KERNEL_CARTESIAN)
    frame_mag_coords = []
    if mag_coords_required:
        coord_path = (data_dir / "shot{}mag_coords_{}_{}_{}.npy"
                        .format(shot, t_start, t_stop, dt_frame))
        if coord_path.is_file():
            print("Load magnetic coordinates from file")
            frame_mag_coords = np.load(str(coord_path))
        else:
            raise(ValueError("provide magnetic coordinates!"))


    # calculate emission fits for frames
    frame_fits = []
    if fast_mode:
        if kernel_function == KERNEL_CARTESIAN_VAR_Z:
            frame_fits = calculate_timeseries_fast(frame_measurements, params,
                                                    x_point_up)
        else:
            raise(ValueError("fast mode only works with KERNEL_CARTESIAN_VAR_Z"))
    else :
        frame_fits = calculate_timeseries(  frame_measurements,
                                            params,
                                            frame_mag_coords,
                                            kernel_function,
                                            x_point_up  )


    # handle different save scenarios
    if save_options:
        static_scale = save_options["static_scale"]
        slowdown = save_options["slowdown"]
        titles = save_options["title"]

        if np.isscalar(static_scale):
            value = "static" if static_scale else "dynamic"
            file_name = ("{}movie_{}_{}_{}x_{}_{}_{}.mp4"
                            .format(shot, kernel_function, value, slowdown, 
                                    t_start, t_stop, dt_frame))
            if "file_name" in save_options:
                file_name = save_options["file_name"]
            print(str(result_dir/file_name))
            save_time_series(str(result_dir/file_name), frame_fits, titles,
                             slowdown=slowdown, static_scale=static_scale)
        else:
            for i in range(len(static_scale)):
                print("Animation {}".format(i+1))
                value = "static" if static_scale[i] else "dynamic"
                file_name = ("{}movie_{}_{}_{}x_{}_{}_{}.mp4"
                            .format(shot, kernel_function, value, slowdown[i], 
                                    t_start, t_stop, dt_frame))
                if "file_name" in save_options:
                    file_name = save_options["file_name"][i]
                save_time_series(str(result_dir/file_name), frame_fits, titles[i],
                                    slowdown=slowdown[i], 
                                    static_scale = static_scale[i])

    else:
        file_name = ("{}movie_{}_static_1x_{}_{}_{}.mp4"
                        .format(shot, kernel_function, t_start, t_stop, 
                                dt_frame))
        save_time_series(str(result_dir/file_name), frame_fits, 
                         "Shot {}".format(shot)) 
