STEPS_PER_SECOND_BOLOMETER = 2500
STEPS_PER_SECOND_NBI = 1000


# static params were determined by optimizing the tomograms from the actual 
# data with regard to (NRMSE and structural similarity index) to replicate 
# those of Pierre. Then the average (some outliers were discarded) over them 
# was calculated to get the parameters below. For the last one the average 
# of both (NRMSE and SSIM) data sets was used. The params are for the combined
# kernel with variable z lengthscale, but they also work quite well for the 
# cartesian kernel with variable z lengthscale when taking only the first 
# 3 values
PARAMS_STATIC_COMB = [138700, 0.0688, 0.2641, 14960, 0.134, 11.2]
PARAMS_STATIC_CART = [148900, 0.0693, 0.2464]
#PARAMS_STATIC_SSIM = [174070, 0.135, 0.263, 38065, 0.237, 6.26]
#PARAMS_STATIC_BOTH = [163241, 0.108, 0.270, 30204, 0.209, 6.65] 

import numpy as np
mask = np.loadtxt("data/mask.txt")